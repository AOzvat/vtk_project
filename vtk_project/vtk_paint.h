﻿#ifndef VTK_PAINT_H
#define VTK_PAINT_H

#include <QWidget>
#include <QtWidgets>
#include <algorithm>
#include "ui_vtk_project.h"

typedef struct {
	float x;
	float y;
	float z;
	QColor color;
}suradnice;

typedef struct {
	int prvy;
	int druhy;
	int treti;
}body_poly;

class vtk_paint : public QWidget {
	Q_OBJECT

public:
	vtk_paint(Ui::vtk_projectClass *parentUi, QWidget *parent = 0);
	~vtk_paint();

	bool new_image(int x, int y);

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

public slots:
	void clear_image();
	void vymaz_vsetko();

	void rovnobezne();
	void stredove(float vzdialenost = 100);

	void generuj_elipsoid(float a = 5, float b = 10, int pocet_rovnobeziek = 5, int pocet_poludnikov = 5);
	void generuj_bezier(int dlzka_vektora = 100, int delenie_vektora = 10, int delenie_krivny = 10);
	void bezierova_krivka(int delenie_krivky);

	void prirad_body_poly(std::vector<suradnice> b, std::vector<body_poly> p, float vzdialenost = 100);
	void nacitaj_osvetlovacie_veci(std::vector<suradnice> svetelne_luce_prirad, QColor farba_zdroj_prirad, QColor farba_okolie_prirad, suradnice difuzia_prirad, suradnice zrkadlova_prirad, int ostrost_prirad, suradnice ambientna_prirad);

	void prepocitaj_azimut_zenit(int azimut, int zenit, bool rovnobeznost = true, float vzdialenost = 100);
	void prirad_uhol_rotacii(int uhol, bool rovnobeznost = true, float vzdialenost = 100);
	suradnice rotuj(suradnice povodna);

	void dda(QPoint prvy, QPoint druhy);
	void kresli();
	void draw_point(int x, int y, QColor farba);

	void konstantne_tienovanie();
	void vypln(std::vector<suradnice> trojuholnik, QColor vysledna_farba);
	void vypln(std::vector<suradnice> trojuholnik);
	void vykresli_medzi(QColor vysledna_farba, int ya, suradnice odkial, suradnice pokial);
	void vykresli_medzi(int ya, suradnice odkial, suradnice pokial);

	QColor vysledna_intenzita(suradnice normala_bodu_plosky);
	QColor vysledna_intenzita_bez_zrkadlovej(suradnice normala_bodu);
	QColor interpolacia_farbe_podla_x(suradnice prvy, suradnice druhy, suradnice aktualny);
	QColor interpolacia_farbe_podla_y(suradnice prvy, suradnice druhy, suradnice aktualny);

	void gouraudovo_tienovanie();

	suradnice vypocet_normaly(int i);
	suradnice normovanie(suradnice normovat);
	void vypocet_normal_vrcholov();

	bool mimo_platna(suradnice bod);
	int kontrola_zlozky(float zlozka);

protected:
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;

private:
	Ui::vtk_projectClass *ui;
	void resizeImage(QImage *image, const QSize &newSize);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;

	QColor **F;
	float **Z;

	std::vector<QPoint> body_nacitane;
	std::vector<QPoint> body_bezier_qpoint;
	std::vector<suradnice> body_bezier;

	QPoint bod_posunu;

	std::vector<suradnice> body_original;
	std::vector<suradnice> body_prepocitane;
	std::vector<body_poly> polygony;
	std::vector<suradnice> body_kresli;

	suradnice vektor_pohladu;
	int zoom;
	int uhol_rotacii;

	std::vector<suradnice> svetelne_luce;
	QColor farba_zdroj;
	QColor farba_okolie;
	suradnice difuzia; 
	suradnice zrkadlova; 
	int ostrost;
	suradnice ambientna;

	std::vector<QColor> farby_bodov;
};

#endif //VTK_PAINT_H