﻿#include "vtk_paint.h"

vtk_paint::vtk_paint(Ui::vtk_projectClass *parentUi, QWidget * parent) : QWidget(parent) {
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
	zoom = 2;

	bod_posunu.setX(0);
	bod_posunu.setY(0);

	uhol_rotacii = 0;

	ui = parentUi;
}

vtk_paint::~vtk_paint() {
	
}

bool vtk_paint::new_image(int x, int y) {
	QImage loadedImage(x, y, QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

void vtk_paint::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void vtk_paint::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void vtk_paint::mousePressEvent(QMouseEvent *event)
{
	//lavy klik mysi
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		draw_point(lastPoint.x(), lastPoint.y(), myPenColor);

		//aby nebola plocha prilis velka a bola umiestnena v strede
		lastPoint.setX((lastPoint.x() / 3) - 100);
		lastPoint.setY((lastPoint.y() / 3) - 100);

		body_nacitane.push_back(lastPoint);
	}

	//pravy klik mysi
	if (event->button() == Qt::RightButton) {
		//odchyti a uchova sa bod kde sa ma posunut objekt
		lastPoint = event->pos();
		draw_point(lastPoint.x(), lastPoint.y(), myPenColor);

		bod_posunu = lastPoint;

		//prepocet s povodnym bodom
		int tmp_x = bod_posunu.x() - body_prepocitane[0].x - image.width() / 2;
		int tmp_y = bod_posunu.y() - body_prepocitane[0].y - image.height() / 2;

		bod_posunu.setX(tmp_x);
		bod_posunu.setY(tmp_y);

		//vymaze kresliacu plochu
		clear_image();

		//rovnobezne ci stavebne
		//HNUSNY pristup k ui cudzej triede (naco potom robime v 2 triedach?!)
		if (ui->comboBox_2->currentIndex() == 0) {
			rovnobezne();
		}
		else {
			stredove(ui->lineEdit->text().toFloat());
		}
	}

	update();
}

void vtk_paint::wheelEvent(QWheelEvent *event)
{
	if (event->delta() > 0)
	{
		//aby nezmizol objekt
		if (zoom > 1) {
			zoom--;
		}
	}
	else
	{
		zoom++;
	}

	//vymaze kresliacu plochu
	clear_image();

	//rovnobezne ci stavebne
	//HNUSNY pristup k ui cudzej triede (naco potom robime v 2 triedach?!)
	if (ui->comboBox_2->currentIndex() == 0) {
		rovnobezne();
	}
	else {
		stredove(ui->lineEdit->text().toFloat());
	}

	update();
}

void vtk_paint::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void vtk_paint::clear_image() {
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void vtk_paint::vymaz_vsetko() {
	printf("Vymazavam vsetko... \n");

	clear_image();
	body_original.clear();
	polygony.clear();
	body_prepocitane.clear();
	body_kresli.clear();

	bod_posunu.setX(0);
	bod_posunu.setY(0);

	uhol_rotacii = 0;

	body_nacitane.clear();
	body_bezier_qpoint.clear();
	body_bezier.clear();

	zoom = 2;

	svetelne_luce.clear();
	farby_bodov.clear();
}

void vtk_paint::rovnobezne() {
	int sirka_pol = image.width() / 2;
	int vyska_pol = image.height() / 2;

	//premaze body pre kreslenie
	body_kresli.clear();

	//prepocita body, aby stred bol v strede platna a z = 0
	for (int i = 0; i < body_prepocitane.size(); i++) {
		suradnice tmp;
		tmp.x = (zoom * body_prepocitane[i].x) + sirka_pol + bod_posunu.x();
		tmp.y = (zoom * body_prepocitane[i].y) + vyska_pol + bod_posunu.y();
		tmp.z = 0;

		tmp = rotuj(tmp);

		body_kresli.push_back(tmp);
	}

	//vykresli
	kresli();
}

void vtk_paint::stredove(float vzdialenost){
	int sirka_pol = image.width() / 2;
	int vyska_pol = image.height() / 2;

	//premaze body pre kreslenie
	body_kresli.clear();

	//prepocita body, aby stred bol v strede platna a z = 0
	for (int i = 0; i < body_prepocitane.size(); i++) {
		suradnice tmp;
		tmp.x = (zoom * ((vzdialenost * body_prepocitane[i].x) / (vzdialenost + body_prepocitane[i].z))) + sirka_pol + bod_posunu.x();
		tmp.y = (zoom * ((vzdialenost * body_prepocitane[i].y) / (vzdialenost + body_prepocitane[i].z))) + vyska_pol + bod_posunu.y();
		tmp.z = 0;

		tmp = rotuj(tmp);

		body_kresli.push_back(tmp);
	}

	//vykresli
	kresli();
}

void vtk_paint::generuj_elipsoid(float a, float b, int pocet_rovnobeziek, int pocet_poludnikov){
	printf("Generujem elipsoid... \n");

	std::vector<suradnice> body;

	//misi sa zvacsit o 1, aby vykreslovalo spravne
	pocet_rovnobeziek++;

	float fi, theta;
	float fi_zvacs = 2 * M_PI / pocet_poludnikov;
	float theta_zvacs = M_PI / pocet_rovnobeziek;

	//vypocita presecniky
	int pocet_priesecnikov = (pocet_rovnobeziek + 1) * pocet_poludnikov;

	//nastavi dlzku vectora
	body.resize(pocet_priesecnikov);

	//otvorenie suboru na pisanie a moznost zadania nazvu suboru
	QString nazov_suboru = QFileDialog::getSaveFileName(this, "Save as:", "", "vtk (*.vtk)");
	QFile vtk_file(nazov_suboru);
	vtk_file.open(QFile::WriteOnly | QFile::Text);
	QTextStream file(&vtk_file);

	//"hlavicka " vtk
	file << "# vtk DataFile Version 3.0" << endl;
	file << "vtk output" << endl;
	file << "ASCII" << endl;
	file << "DATASET POLYDATA" << endl;

	//vypocet bodov
	theta = 0;
	for (int i = 0; i <= pocet_rovnobeziek; i++) {
		fi = 0;
		for (int j = 0; j < pocet_poludnikov; j++) {
			body[pocet_poludnikov * i + j].x = a * sin(theta) * cos(fi);
			body[pocet_poludnikov * i + j].y = a * sin(theta) * sin(fi);
			body[pocet_poludnikov * i + j].z = b * cos(theta);

			fi += fi_zvacs;
		}
		theta += theta_zvacs;
	}

	//"hlavicka" pre body
	file << "POINTS " << (pocet_priesecnikov - (pocet_poludnikov * 2) + 2) << " float" << endl;

	//zapis bodov
	//hranica sa divnie aby sa nezapisoval viackrat ten isty bod, na poloch
	for (int i = (pocet_poludnikov - 1); i < (pocet_priesecnikov - pocet_poludnikov + 1); i++) {
		file << 10 * body[i].x << " " << 10 * body[i].y << " " << 10 * body[i].z << endl;
	}

	//kedze sme zvacsili o 1, misime aj znemsit
	pocet_rovnobeziek--;

	//"hlavicka" pre trojuhaolniky
	file << "POLYGONS " << (pocet_poludnikov * pocet_rovnobeziek) * 2 << " " << ((pocet_poludnikov * pocet_rovnobeziek) * 2) * 4 << endl;

	//spajanie trojuholnikov
	//najprv vrch
	for (int i = 1; i < pocet_poludnikov; i++) {
		file << 3 << " " << i + 1 << " " << i << " " << 0 << endl;
	}
	file << 3 << " " << 1 << " " << pocet_poludnikov << " " << 0 << endl;

	//potom stred
	for (int i = 1; i <= pocet_poludnikov * pocet_rovnobeziek - pocet_rovnobeziek; i++) {
		//prechod na novy riadok
		if ((i % pocet_poludnikov == pocet_poludnikov) || (i % pocet_poludnikov == 0)) {
			file << 3 << " " << i << " " << i + 1 - pocet_poludnikov << " " << i + pocet_poludnikov << endl;
			file << 3 << " " << i + 1 - pocet_poludnikov << " " << i + 1 << " " << i + pocet_poludnikov << endl;
		}
		//v starom riadku
		else {
			file << 3 << " " << i << " " << i + 1 << " " << i + pocet_poludnikov << endl;
			file << 3 << " " << i + 1 << " " << i + 1 + pocet_poludnikov << " " << i + pocet_poludnikov << endl;
		}
	}

	//nakoniec koniec
	for (int i = pocet_poludnikov * pocet_rovnobeziek - pocet_rovnobeziek + 1; i < (pocet_poludnikov * pocet_rovnobeziek); i++) {
		file << 3 << " " << i << " " << i + 1 << " " << (pocet_priesecnikov - (pocet_poludnikov * 2) + 2) - 1 << endl;
	}
	file << 3 << " " << (pocet_poludnikov * pocet_rovnobeziek) << " " << pocet_poludnikov * pocet_rovnobeziek - pocet_rovnobeziek + 1 << " " << (pocet_priesecnikov - (pocet_poludnikov * 2) + 2) - 1 << endl;

	//zatvorenie suboru
	vtk_file.close();
}

void vtk_paint::generuj_bezier(int dlzka_vektora, int delenie_vektora, int delenie_krivny) {
	printf("Generujem Bezierovu plochu... \n");

	//vypocita sa Bezierova krivka
	bezierova_krivka(delenie_krivny);

	//o kolko sa bude posuvat generovanie krivky po vektore ("elipsoid- rovnobezky")
	int z_plus = dlzka_vektora / delenie_vektora;

	for (int z_suradnica = 0; z_suradnica <= dlzka_vektora; z_suradnica += z_plus) {
		//zapamatanie bodov Bezierovej krivky aj so z-ovou suradnicou
		for (int i = 0; i < body_bezier_qpoint.size(); i++) {
			suradnice tmp;
			tmp.x = body_bezier_qpoint[i].x();
			tmp.y = body_bezier_qpoint[i].y();
			tmp.z = z_suradnica;

			body_bezier.push_back(tmp);
		}
	}

	//otvorenie suboru na pisanie a moznost zadania nazvu suboru
	QString nazov_suboru = QFileDialog::getSaveFileName(this, "Save as:", "", "vtk (*.vtk)");
	QFile vtk_file(nazov_suboru);
	vtk_file.open(QFile::WriteOnly | QFile::Text);
	QTextStream file(&vtk_file);

	//"hlavicka " vtk
	file << "# vtk DataFile Version 3.0" << endl;
	file << "vtk output" << endl;
	file << "ASCII" << endl;
	file << "DATASET POLYDATA" << endl;

	//"hlavicka" pre body
	file << "POINTS " << body_bezier.size() << " float" << endl;

	//zapis bodov
	for (int i = 0; i < body_bezier.size(); i++) {
		file << body_bezier[i].x << " " << body_bezier[i].y << " " << body_bezier[i].z << endl;
	}

	//"hlavicka" pre trojuhaolniky
	file << "POLYGONS " << (delenie_krivny * delenie_vektora) * 2 << " " << ((delenie_krivny * delenie_vektora) * 2) * 4 << endl;

	//spajanie trojuholnikov
	for (int i = 0; i < (delenie_krivny * delenie_vektora) + delenie_vektora; i++) {
		//ak sa nenachadzame na konci riadku tak sa vykona zapis
		if (body_bezier[i].z == body_bezier[i + 1].z) {
			file << 3 << " " << i << " " << i + 1 << " " << i + (delenie_krivny + 1) << endl;
			file << 3 << " " << i + 1 << " " << i + 1 + (delenie_krivny + 1) << " " << i + (delenie_krivny + 1) << endl;
		}
	}

	//zatvorenie suboru
	vtk_file.close();
}

void vtk_paint::bezierova_krivka(int delenie_krivky) {
	//vypocita sa Bezierova krivka bez z-ovej suradnice
	int n = body_nacitane.size();
	float t = 0;
	QPointF prvy, druhy;
	std::vector<QPointF> P1;
	std::vector<QPointF> P2;

	//nastavenie velkosti 
	P1.resize(n);
	P2.resize(n);

	//priradi k P1 povodbe body
	for (int k = 0; k < n; k++) {
		P1[k] = (QPointF)body_nacitane[k];
	}

	//prvy bod pre "dda"
	prvy = P1[0];

	//prejde celu krivku podelenu na pocet_deleni casti
	for (int delenie = 0; delenie <= delenie_krivky; delenie++) {
		//vypocet krociku
		t = delenie * (1.0 / delenie_krivky);

		//to je ten vypocet
		for (int j = 1; j < n; j++) {
			for (int i = 0; i < n - j; i++) {
				P2[i] = (1 - t) * P1[i] + t *  P1[i + 1];
			}
			P1 = P2;
		}

		//nastavy druhy bod, vykona zapis do body_bezier a zas nastavy prvy bod
		druhy = P1[0];
		body_bezier_qpoint.push_back(druhy.toPoint());
		prvy = P1[0];

		//priradi k P1 povodbe body
		for (int k = 0; k < n; k++) {
			P1[k] = (QPointF)body_nacitane[k];
		}
	}

	//vymaze vektory
	P1.clear();
	P2.clear();
}

void vtk_paint::prirad_body_poly(std::vector<suradnice> b, std::vector<body_poly> p, float vzdialenost) {
	//priradi body a polygony s vtk_project

	body_original = b;
	body_prepocitane = b;
	polygony = p;

	//nastavenie vektora pohladu a normovanie
	//(nepatri sem, ale nek 100 krat neposielam vzdialenost)
	vektor_pohladu.x = 0;
	vektor_pohladu.y = 0;
	vektor_pohladu.z = vzdialenost;

	vektor_pohladu = normovanie(vektor_pohladu);
}

void vtk_paint::nacitaj_osvetlovacie_veci(std::vector<suradnice> svetelne_luce_prirad, QColor farba_zdroj_prirad, QColor farba_okolie_prirad, suradnice difuzia_prirad, suradnice zrkadlova_prirad, int ostrost_prirad, suradnice ambientna_prirad) {
	svetelne_luce = svetelne_luce_prirad;

	farba_zdroj = farba_zdroj_prirad;
	farba_okolie = farba_okolie_prirad;

	difuzia = difuzia_prirad;
	zrkadlova = zrkadlova_prirad;
	ambientna = ambientna_prirad;
	ostrost = ostrost_prirad;
}

void vtk_paint::prepocitaj_azimut_zenit(int azimut, int zenit, bool rovnobeznost, float vzdialenost) {
	float x = 0, y = 0, z = 0;
	float hodnota_rad_azimut = 0, hodnota_rad_zenit = 0;

	//prevedie stupne do riadianov
	hodnota_rad_azimut = azimut *(M_PI / 180);
	hodnota_rad_zenit = zenit *(M_PI / 180);

	//proti smeru hodinovych ruciciek
	for (int i = 0; i < body_original.size(); i++) {
		x = body_original[i].x * cos(hodnota_rad_azimut) - body_original[i].y * sin(hodnota_rad_azimut);
		y = body_original[i].x * sin(hodnota_rad_azimut) * cos(hodnota_rad_zenit) + body_original[i].y * cos(hodnota_rad_azimut) * cos(hodnota_rad_zenit) - body_original[i].z * sin(hodnota_rad_zenit);
		z = body_original[i].x * sin(hodnota_rad_azimut) * sin(hodnota_rad_zenit) + body_original[i].y * cos(hodnota_rad_azimut) * sin(hodnota_rad_zenit) + body_original[i].z * cos(hodnota_rad_zenit);

		body_prepocitane[i].x = x;
		body_prepocitane[i].y = y;
		body_prepocitane[i].z = z;
	}

	//rovnobezne ci stavebne
	if (rovnobeznost == true) {
		rovnobezne();
	}
	else {
		stredove(vzdialenost);
	}

	update();
}

void vtk_paint::prirad_uhol_rotacii(int uhol, bool rovnobeznost, float vzdialenost) {
	uhol_rotacii = uhol;

	//rovnobezne ci stavebne
	if (rovnobeznost == true) {
		rovnobezne();
	}
	else {
		stredove(vzdialenost);
	}
}

suradnice vtk_paint::rotuj(suradnice povodna) {
	//prevedie stupne do riadianov
	float hodnota_rad = uhol_rotacii * (M_PI / 180);

	//bod okolo ktoreho ideme otacat
	float sx = image.width() / 2;
	float sy = image.height() / 2;

	suradnice tmp;
	tmp.x = (povodna.x - sx) * cos(hodnota_rad) + (povodna.y - sy) * sin(hodnota_rad) + sx;
	tmp.y = -(povodna.x - sx) * sin(hodnota_rad) + (povodna.y - sy) * cos(hodnota_rad) + sy;
	tmp.z = 0;

	return tmp;
}

void vtk_paint::dda(QPoint prvy, QPoint druhy) {
	//vykresli na obrazovku
	int x1, y1, x2, y2, dx, dy, krok;
	float zvacsene_x, zvacsene_y, x, y;

	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	x1 = prvy.x();
	y1 = prvy.y();
	x2 = druhy.x();
	y2 = druhy.y();

	//vypocita rozdiel medzi bodmi
	dx = x2 - x1;
	dy = y2 - y1;

	//pre zaciatok pripadi zaciatocny bod
	x = x1;
	y = y1;

	//rozhodne sa v ktorom smere sa bude krokuvat
	if (abs(dx) > abs(dy)) {
		krok = abs(dx);
	}
	else {
		krok = abs(dy);
	}

	//o kolko sa budu suradnice zvacsovat
	// if dx > dy -> zvacsenie_x = 1
	// if dx < dy -> zvacsenie_y = 1
	zvacsene_x = dx / (float)krok;
	zvacsene_y = dy / (float)krok;

	//zvacsovanie a vykreslovanie "ciare"
	for (int j = 0; j < krok; j++) {
		x += zvacsene_x;
		y += zvacsene_y;
		painter.drawPoint(x, y);
	}

	update();
}

void vtk_paint::kresli() {
	//vykresli body
	for (int i = 0; i < polygony.size(); i++) {
		//suradnice prvyho bodu za dda
		QPoint tmp1((int)(body_kresli[polygony[i].prvy].x), (int)(body_kresli[polygony[i].prvy].y));

		//suradnice druhyho bodu za dda
		QPoint tmp2((int)(body_kresli[polygony[i].druhy].x), (int)(body_kresli[polygony[i].druhy].y));

		//suradnice tretieho bodu za dda
		QPoint tmp3((int)(body_kresli[polygony[i].treti].x), (int)(body_kresli[polygony[i].treti].y));

		dda(tmp1, tmp2);
		dda(tmp2, tmp3);
		dda(tmp3, tmp1);
	}
}

void vtk_paint::draw_point(int x, int y, QColor farba) {
	//vykresli jeden bod, lebo ked som pouzila painter v dvoch funkciach,
	//tak mi hovorilo, ze sa nesmie pouzivat v dvoch funkciach

	QPainter painter(&image);
	painter.setPen(QPen(QColor(farba), myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	painter.drawPoint(x, y);
}

bool sort_podla_y(const suradnice &a, const suradnice &b) {
	return a.y < b.y;
}

bool sort_podla_x(const suradnice &a, const suradnice &b) {
	return a.x < b.x;
}

//obcas vykresli ciaru ktora nesplna z-buffer (-86.6 > 86.6) alebo vykresli (86 == 86)
void vtk_paint::konstantne_tienovanie() {
	printf("Robim konstantne tienovanie... \n");

	int sirka = image.width();
	int vyska = image.height();

	//vytvorenie dvojrozmerneho pola pre uladanie z suradnici
	Z = new float *[sirka];

	for (int i = 0; i < sirka; i++) {
		Z[i] = new float[vyska];
	}

	//naplanenie tychto poli
	for (int i = 0; i < sirka; i++) {
		for (int j = 0; j < vyska; j++) {
			Z[i][j] = (HUGE_VALF);
		}
	}

	//prejde vsetky trojuholniky
	for (int i = 0; i < polygony.size(); i++){
//	for (int i = 0; i < 15; i++){							//KED KCEME ZMENSIT POCET POLYGONOV
		std::vector <suradnice> body_trojuholnika;
		suradnice tmp;
		
		//prvy bod trojuhonika
		tmp.x = body_kresli[polygony[i].prvy].x;
		tmp.y = body_kresli[polygony[i].prvy].y;
		tmp.z = body_prepocitane[polygony[i].prvy].z;
		body_trojuholnika.push_back(tmp);

		//druhy bod trojuholnika
		tmp.x = body_kresli[polygony[i].druhy].x;
		tmp.y = body_kresli[polygony[i].druhy].y;
		tmp.z = body_prepocitane[polygony[i].druhy].z;
		body_trojuholnika.push_back(tmp);

		//treti bod trojuholnika
		tmp.x = body_kresli[polygony[i].treti].x;
		tmp.y = body_kresli[polygony[i].treti].y;
		tmp.z = body_prepocitane[polygony[i].treti].z;
		body_trojuholnika.push_back(tmp);
		
		//vypocita normalu trojuholnika
		suradnice normala = vypocet_normaly(i);

		//nastavy sa farba jedneho trojuholnika
		QColor vysledna_farba = vysledna_intenzita(normala);

		//vysortovanie podla y body_trojuholnika
		qSort(body_trojuholnika.begin(), body_trojuholnika.end(), sort_podla_y);

		//IBA KONTROLA povodbe body
		/*	printf("Povodne body-trojuhonika: \n");
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[0].x, body_trojuholnika[0].y, body_trojuholnika[0].z);
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[1].x, body_trojuholnika[1].y, body_trojuholnika[1].z);
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[2].x, body_trojuholnika[2].y, body_trojuholnika[2].z);
			getchar();*/

		//ak sa dva body nerovnake (1. a 2. aa 2. a 3.)
		if ((body_trojuholnika[0].y != body_trojuholnika[1].y) && (body_trojuholnika[1].y != body_trojuholnika[2].y)) {
			//vytvori sa novy bod a prida sa do body_trojuholnika
			tmp.y = body_trojuholnika[1].y;
			tmp.x = body_trojuholnika[0].x + (tmp.y - body_trojuholnika[0].y) * ((body_trojuholnika[2].x - body_trojuholnika[0].x) / (body_trojuholnika[2].y - body_trojuholnika[0].y));
			tmp.z = body_trojuholnika[0].z + (tmp.y - body_trojuholnika[0].y) * ((body_trojuholnika[2].z - body_trojuholnika[0].z) / (body_trojuholnika[2].y - body_trojuholnika[0].y));
			body_trojuholnika.push_back(tmp);

			//vysortovanie podla y body_trojuholnika
			qSort(body_trojuholnika.begin(), body_trojuholnika.end(), sort_podla_y);

			//IBA KONTROLA, ked sa prida bod
		/*	printf("Body-trojuhonika s pridanym bodom: \n");
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[0].x, body_trojuholnika[0].y, body_trojuholnika[0].z);
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[1].x, body_trojuholnika[1].y, body_trojuholnika[1].z);
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[2].x, body_trojuholnika[2].y, body_trojuholnika[2].z);
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[3].x, body_trojuholnika[3].y, body_trojuholnika[3].z);
			getchar();*/

			std::vector <suradnice> horny_trojuholnik;
			horny_trojuholnik.resize(3);

			//nacitanie bodov horneho trojuholnika
			for (int j = 0; j < body_trojuholnika.size() - 1; j++) {
				horny_trojuholnik[j].x = body_trojuholnika[j].x;
				horny_trojuholnik[j].y = body_trojuholnika[j].y;
				horny_trojuholnik[j].z = body_trojuholnika[j].z;
			}

			std::vector <suradnice> dolny_trojuholnik;
			dolny_trojuholnik.resize(3);

			//nacitanie bodov dolneho trojuhonika
			for (int j = 1; j < body_trojuholnika.size(); j++) {
				dolny_trojuholnik[j - 1].x = body_trojuholnika[j].x;
				dolny_trojuholnik[j - 1].y = body_trojuholnika[j].y;
				dolny_trojuholnik[j - 1].z = body_trojuholnika[j].z;
			}

			//vyplnenie dolneho trojuholnika
			vypln(dolny_trojuholnik, vysledna_farba);

			//vyplnenie horneho trojuholnika
			vypln(horny_trojuholnik, vysledna_farba);
		}
		//ak sa rovna y bodu 1 a bodu 2 ALEBO ak sa rovna y bodu 2 a bodu 3
		//vytvori sa iba jeden trojuholnik, tj. nevytvori sa nist, lebo uz existuje ako body_trojuholnika
		else {
			//vyplnenie toho jedneho trojuholnika
			vypln(body_trojuholnika, vysledna_farba);
		}
	}

	//dealokacia poli
	for (int i = 0; i < image.height(); i++) {
		delete[] Z[i];
	}

	delete[] Z;
}

void vtk_paint::vypln(std::vector<suradnice> trojuholnik, QColor vysledna_farba) {
	//vyplni dany trojuholnik

	//IBA KONTROLA aky trojuholnik mame prave
	/*		printf("Zoradene podla y: \n");
			printf("X: %f, Y: %f, Z: %f \n", trojuholnik[0].x, trojuholnik[0].y, trojuholnik[0].z);
			printf("X: %f, Y: %f, Z: %f \n", trojuholnik[1].x, trojuholnik[1].y, trojuholnik[1].z);
			printf("X: %f, Y: %f, Z: %f \n", trojuholnik[2].x, trojuholnik[2].y, trojuholnik[2].z);
			getchar();*/

	//ked je ten trojuholnik obrateny hore hlavou
	if (trojuholnik[0].y == trojuholnik[1].y) {
		std::swap(trojuholnik[0].x, trojuholnik[2].x);
		std::swap(trojuholnik[0].y, trojuholnik[2].y);
		std::swap(trojuholnik[0].z, trojuholnik[2].z);
	}

	//aby vzdy bol 1. bod v trojuholniku s tou mensou x vzhladom na 2. bod toho trojuholnika
	if (trojuholnik[2].x < trojuholnik[1].x) {
		std::swap(trojuholnik[2].x, trojuholnik[1].x);
		std::swap(trojuholnik[2].y, trojuholnik[1].y);
		std::swap(trojuholnik[2].z, trojuholnik[1].z);
	}

	//IBA KONTROLA ci dobre swapuje
	/*		printf("Po swape: \n");
			printf("X: %f, Y: %f, Z: %f \n", trojuholnik[0].x, trojuholnik[0].y, trojuholnik[0].z);
			printf("X: %f, Y: %f, Z: %f \n", trojuholnik[1].x, trojuholnik[1].y, trojuholnik[1].z);
			printf("X: %f, Y: %f, Z: %f \n", trojuholnik[2].x, trojuholnik[2].y, trojuholnik[2].z);
			getchar();*/

	//vypocet obratenej smernice
	float w1, w2;

	//ak sa y suradnice bodou rovnaju, smernica bude ina
	if ((int)(trojuholnik[1].y + 0.5) == (int)(trojuholnik[0].y + 0.5)) {
		w1 = 1;
	}
	//ak nie su rovnake sa dopocita podla vzorcu
	else {
		w1 = (trojuholnik[1].x - trojuholnik[0].x) / (float)(trojuholnik[1].y - trojuholnik[0].y);
	}

	//ak sa y suradnice bodou rovnaju, smernica bude ina
	if ((int)(trojuholnik[2].y + 0.5) == (int)(trojuholnik[0].y + 0.5)) {
		w2 = 1;
	}
	//ak nie su rovnake sa dopocita podla vzorcu
	else {
		w2 = (trojuholnik[2].x - trojuholnik[0].x) / (float)(trojuholnik[2].y - trojuholnik[0].y);
	}

	//vytriedime podla x a nasledne podla w
	//je to dane natvrdo, 1. hrana je s indexok 0 a 1, 2. hrana je 0 a 2

	int ymin = 0;
	int ymax = 0;
	
	//vykreslenie prveho bodu alebo riadku
	//ak je trojuholnik /\ spicom hore
	if (trojuholnik[0].y < trojuholnik[1].y) {
		//kontrola so z-bufferom a ci je mimo platna
		if (!mimo_platna(trojuholnik[0])) {
			if (Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)] > trojuholnik[0].z) {
				Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)] = trojuholnik[0].z;
				draw_point((int)(trojuholnik[0].x + 0.5), (int)(trojuholnik[0].y + 0.5), vysledna_farba);
			}
			//IBA KONTROLA z_buffera
			/*		QMessageBox box;
					box.setText("Z-buffer: " + QString::number(Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)]) + "\n z-ova bod: " + QString::number(trojuholnik[0].z));
					box.exec();*/
		}

		//nacitanie odkial pojde vyplnanie (y horneho bodu + 1 lebo riadok nizsie) a ymax
		ymin = (int)(trojuholnik[0].y + 1.5);
		ymax = (int)(trojuholnik[1].y + 0.5);
	}
	//ak je trojuholnik \/ spicom dolu
	else {
		//vykresli sa prvy bod hornej priamky + z-buffer + mimo_platna
		if (!mimo_platna(trojuholnik[1])) {
			if (Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)] > trojuholnik[1].z) {
				Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)] = trojuholnik[1].z;
				draw_point((int)(trojuholnik[1].x + 0.5), (int)(trojuholnik[1].y + 0.5), vysledna_farba);
			}
			//IBA KONTROLA z_buffera
			/*		QMessageBox box;
					box.setText("Z-buffer: " + QString::number(Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)]) + "\n z-ova bod: " + QString::number(trojuholnik[1].z));
					box.exec(); */
		}

		//vykresli sa posledny bod hornej priamky + z-buffer + mimo_platna
		if (!mimo_platna(trojuholnik[2])) {
			if (Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)] > trojuholnik[2].z) {
				Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)] = trojuholnik[2].z;
				draw_point((int)(trojuholnik[2].x + 0.5), (int)(trojuholnik[2].y + 0.5), vysledna_farba);
			}
			//IBA KONTROLA z_buffera
			/*		QMessageBox box;
					box.setText("Z-buffer: " + QString::number(Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)]) + "\n z-ova bod: " + QString::number(trojuholnik[2].z));
					box.exec();*/
		}

		//vykreslenie medzi bodami
		vykresli_medzi(vysledna_farba, (int)(trojuholnik[1].y + 0.5), trojuholnik[1], trojuholnik[2]);

		//nacitanie odkial pojde vyplnanie (y horneho bodu + 1 lebo riadok nizsie) a ymax
		ymin = (int)(trojuholnik[1].y + 1.5);
		ymax = (int)(trojuholnik[0].y + 0.5);
	}

	//obcas priesecnike neexistuju a preto ich nebudeme pocitat
	if (ymin != ymax) {
		suradnice priesecnik1, priesecnik2;

		//vypocet priesecnikov
		//ak je trojuholnik /\ spicom hore
		if (trojuholnik[0].y < trojuholnik[1].y) {
			priesecnik1.x = w1 * ymin - w1 * trojuholnik[0].y + trojuholnik[0].x;
			priesecnik2.x = w2 * ymin - w2 * trojuholnik[0].y + trojuholnik[0].x;

			priesecnik1.y = ymin;
			priesecnik2.y = ymin;

			priesecnik1.z = trojuholnik[0].z + (priesecnik1.y - trojuholnik[0].y) * ((trojuholnik[1].z - trojuholnik[0].z) / (trojuholnik[1].y - trojuholnik[0].y));
			priesecnik2.z = trojuholnik[0].z + (priesecnik2.y - trojuholnik[0].y) * ((trojuholnik[2].z - trojuholnik[0].z) / (trojuholnik[2].y - trojuholnik[0].y));
		}
		//ak je trojuholnik \/ spicom dolu
		else {
			priesecnik1.x = w1 * ymin - w1 * trojuholnik[1].y + trojuholnik[1].x;
			priesecnik2.x = w2 * ymin - w2 * trojuholnik[2].y + trojuholnik[2].x;

			priesecnik1.y = ymin;
			priesecnik2.y = ymin;

			priesecnik1.z = trojuholnik[1].z + (priesecnik1.y - trojuholnik[1].y) * ((trojuholnik[0].z - trojuholnik[1].z) / (trojuholnik[0].y - trojuholnik[1].y));
			priesecnik2.z = trojuholnik[2].z + (priesecnik2.y - trojuholnik[2].y) * ((trojuholnik[0].z - trojuholnik[2].z) / (trojuholnik[0].y - trojuholnik[2].y));
		}

		//vykresli priestor medzi priesecnikami
		vykresli_medzi(vysledna_farba, ymin, priesecnik1, priesecnik2);

		//pripocitovanie priesecnika, a vykreslenie bodov medzi priesecnikom
		for (int ya = ymin + 1; ya < ymax; ya++) {
			priesecnik1.x += w1;
			priesecnik2.x += w2;

			vykresli_medzi(vysledna_farba, ya, priesecnik1, priesecnik2);

		}
	}

	//asi to netreba
	/*
	//vykreslenie posledneho bodu alebo riadku
	//ak je trojuholnik \/ spicom dolu
	if (trojuholnik[0].y < trojuholnik[1].y) {
		//kontrola so z-bufferom a ci je mimo platna
		if (!mimo_platna(trojuholnik[0])) {
			if (Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)] > trojuholnik[0].z) {
				Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)] = trojuholnik[0].z;
				draw_point((int)(trojuholnik[0].x + 0.5), (int)(trojuholnik[0].y + 0.5), vysledna_farba);
			}
		}
	}
	//ak je trojuholnik /\ spicom hore
	else {
		//vykresli sa prvy bod dolnej priamky + z-buffer + mimo_platna
		if (!mimo_platna(trojuholnik[1])) {
			if (Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)] > trojuholnik[1].z) {
				Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)] = trojuholnik[1].z;
				draw_point((int)(trojuholnik[1].x + 0.5), (int)(trojuholnik[1].y + 0.5), vysledna_farba);
			}
		}

		//vykresli sa posledny bod dolnej priamky + z-buffer + mimo_platna
		if (!mimo_platna(trojuholnik[2])) {
			if (Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)] > trojuholnik[1].z) {
				Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)] = trojuholnik[1].z;
				draw_point((int)(trojuholnik[1].x + 0.5), (int)(trojuholnik[1].y + 0.5), vysledna_farba);
			}
		}

		//vykreslenie medzi bodami
		vykresli_medzi(vysledna_farba, (int)(trojuholnik[1].y + 0.5), trojuholnik[1], trojuholnik[2]);
	}
	*/
}

void vtk_paint::vypln(std::vector<suradnice> trojuholnik) {
	//vyplni dany trojuholnik

	//IBA KONTROLA aky trojuholnik mame prave
	/*	printf("Zoradene podla y: \n");
		printf("X: %f, Y: %f, Z: %f \n", trojuholnik[0].x, trojuholnik[0].y, trojuholnik[0].z);
		printf("Farba: %d %d %d \n", trojuholnik[0].color.red(), trojuholnik[0].color.green(), trojuholnik[0].color.blue());
		printf("X: %f, Y: %f, Z: %f \n", trojuholnik[1].x, trojuholnik[1].y, trojuholnik[1].z);
		printf("Farba: %d %d %d \n", trojuholnik[1].color.red(), trojuholnik[1].color.green(), trojuholnik[1].color.blue());
		printf("X: %f, Y: %f, Z: %f \n", trojuholnik[2].x, trojuholnik[2].y, trojuholnik[2].z);
		printf("Farba: %d %d %d \n\n", trojuholnik[2].color.red(), trojuholnik[2].color.green(), trojuholnik[2].color.blue());
		getchar();*/

	//ked je ten trojuholnik obrateny hore hlavou
	if (trojuholnik[0].y == trojuholnik[1].y) {
		std::swap(trojuholnik[0].x, trojuholnik[2].x);
		std::swap(trojuholnik[0].y, trojuholnik[2].y);
		std::swap(trojuholnik[0].z, trojuholnik[2].z);
		std::swap(trojuholnik[0].color, trojuholnik[2].color);
	}

	//aby vzdy bol 1. bod v trojuholniku s tou mensou x vzhladom na 2. bod toho trojuholnika
	if (trojuholnik[2].x < trojuholnik[1].x) {
		std::swap(trojuholnik[2].x, trojuholnik[1].x);
		std::swap(trojuholnik[2].y, trojuholnik[1].y);
		std::swap(trojuholnik[2].z, trojuholnik[1].z);
		std::swap(trojuholnik[2].color, trojuholnik[1].color);
	}

	//IBA KONTROLA ci dobre swapuje
	/*		printf("Po swape: \n");
			printf("X: %f, Y: %f, Z: %f \n", trojuholnik[0].x, trojuholnik[0].y, trojuholnik[0].z);
			printf("Farba: %d %d %d \n", trojuholnik[0].color.red(), trojuholnik[0].color.green(), trojuholnik[0].color.blue());
			printf("X: %f, Y: %f, Z: %f \n", trojuholnik[1].x, trojuholnik[1].y, trojuholnik[1].z);
			printf("Farba: %d %d %d \n", trojuholnik[1].color.red(), trojuholnik[1].color.green(), trojuholnik[1].color.blue());
			printf("X: %f, Y: %f, Z: %f \n", trojuholnik[2].x, trojuholnik[2].y, trojuholnik[2].z);
			printf("Farba: %d %d %d \n\n", trojuholnik[2].color.red(), trojuholnik[2].color.green(), trojuholnik[2].color.blue());
			getchar();*/

	//vypocet obratenej smernice
	float w1, w2;
	//ak sa y suradnice bodou rovnaju, smernica bude ina
	if ((int)(trojuholnik[1].y + 0.5) == (int)(trojuholnik[0].y + 0.5)) {
		w1 = 0;
	}
	//ak nie su rovnake sa dopocita podla vzorcu
	else {
		w1 = (trojuholnik[1].x - trojuholnik[0].x) / (float)(trojuholnik[1].y - trojuholnik[0].y);
	}

	//ak sa y suradnice bodou rovnaju, smernica bude ina
	if ((int)(trojuholnik[2].y + 0.5) == (int)(trojuholnik[0].y + 0.5)) {
		w2 = 0;
	}
	//ak nie su rovnake sa dopocita podla vzorcu
	else {
		w2 = (trojuholnik[2].x - trojuholnik[0].x) / (float)(trojuholnik[2].y - trojuholnik[0].y);
	}

	//vytriedime podla x a nasledne podla w
	//je to dane natvrdo, 1. hrana je s indexok 0 a 1, 2. hrana je 0 a 2

	int ymin = 0;
	int ymax = 0;

	//vykreslenie prveho bodu alebo riadku
	//ak je trojuholnik /\ spicom hore
	if (trojuholnik[0].y < trojuholnik[1].y) {
		//kontrola so z-bufferom a ci je mimo platna
		if (!mimo_platna(trojuholnik[0])) {
			if (Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)] > trojuholnik[0].z) {
				Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)] = trojuholnik[0].z;
				draw_point((int)(trojuholnik[0].x + 0.5), (int)(trojuholnik[0].y + 0.5), trojuholnik[0].color);
			}
			//IBA KONTROLA z_buffera
			/*		QMessageBox box;
					box.setText("Z-buffer: " + QString::number(Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)]) + "\n > z-ova bod: " + QString::number(trojuholnik[0].z));
					box.exec();*/
		}

		//nacitanie odkial pojde vyplnanie (y horneho bodu + 1 lebo riadok nizsie) a ymax
		ymin = (int)(trojuholnik[0].y + 1.5);
		ymax = (int)(trojuholnik[1].y + 0.5);
	}
	//ak je trojuholnik \/ spicom dolu
	else {
		//vykresli sa prvy bod hornej priamky + z-buffer + mimo_platna
		if (!mimo_platna(trojuholnik[1])) {
			if (Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)] > trojuholnik[1].z) {
				Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)] = trojuholnik[1].z;
				draw_point((int)(trojuholnik[1].x + 0.5), (int)(trojuholnik[1].y + 0.5), trojuholnik[1].color);
			}
			//IBA KONTROLA z_buffera
			/*		QMessageBox box;
					box.setText("Z-buffer: " + QString::number(Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)]) + "\n > z-ova bod: " + QString::number(trojuholnik[1].z));
					box.exec(); */
		}

		//vykresli sa posledny bod hornej priamky + z-buffer + mimo_platna
		if (!mimo_platna(trojuholnik[2])) {
			if (Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)] > trojuholnik[2].z) {
				Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)] = trojuholnik[2].z;
				draw_point((int)(trojuholnik[2].x + 0.5), (int)(trojuholnik[2].y + 0.5), trojuholnik[2].color);
			}
			//IBA KONTROLA z_buffera
			/*		QMessageBox box;
					box.setText("Z-buffer: " + QString::number(Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)]) + "\n > z-ova bod: " + QString::number(trojuholnik[2].z));
					box.exec();*/
		}

		//vykreslenie medzi bodami
		vykresli_medzi((int)(trojuholnik[1].y + 0.5), trojuholnik[1], trojuholnik[2]);

		//nacitanie odkial pojde vyplnanie (y horneho bodu + 1 lebo riadok nizsie) a ymax
		ymin = (int)(trojuholnik[1].y + 1.5);
		ymax = (int)(trojuholnik[0].y + 0.5);
	}

	//obcas priesecnike neexistuju a preto ich nebudeme pocitat
	if (ymin != ymax) {
		suradnice priesecnik1, priesecnik2;

		//vypocet priesecnikov
		//ak je trojuholnik /\ spicom hore
		if (trojuholnik[0].y < trojuholnik[1].y) {
			priesecnik1.x = w1 * ymin - w1 * trojuholnik[0].y + trojuholnik[0].x;
			priesecnik2.x = w2 * ymin - w2 * trojuholnik[0].y + trojuholnik[0].x;

			priesecnik1.y = ymin;
			priesecnik2.y = ymin;

			priesecnik1.z = trojuholnik[0].z + (trojuholnik[1].y - trojuholnik[0].y) * ((trojuholnik[1].z - trojuholnik[0].z) / (trojuholnik[1].y - trojuholnik[0].y));
			priesecnik2.z = trojuholnik[0].z + (trojuholnik[2].y - trojuholnik[0].y) * ((trojuholnik[2].z - trojuholnik[0].z) / (trojuholnik[2].y - trojuholnik[0].y));

			priesecnik1.color = interpolacia_farbe_podla_y(trojuholnik[0], trojuholnik[1], priesecnik1);
			priesecnik2.color = interpolacia_farbe_podla_y(trojuholnik[0], trojuholnik[2], priesecnik1);
		}
		//ak je trojuholnik \/ spicom dolu
		else {
			priesecnik1.x = w1 * ymin - w1 * trojuholnik[1].y + trojuholnik[1].x;
			priesecnik2.x = w2 * ymin - w2 * trojuholnik[2].y + trojuholnik[2].x;

			priesecnik1.y = ymin;
			priesecnik2.y = ymin;

			priesecnik1.z = trojuholnik[1].z + (trojuholnik[0].y - trojuholnik[1].y) * ((trojuholnik[0].z - trojuholnik[1].z) / (trojuholnik[0].y - trojuholnik[1].y));
			priesecnik2.z = trojuholnik[2].z + (trojuholnik[0].y - trojuholnik[2].y) * ((trojuholnik[0].z - trojuholnik[2].z) / (trojuholnik[0].y - trojuholnik[2].y));

			priesecnik1.color = interpolacia_farbe_podla_y(trojuholnik[1], trojuholnik[0], priesecnik1);
			priesecnik2.color = interpolacia_farbe_podla_y(trojuholnik[2], trojuholnik[0], priesecnik1);
		}

		//vykresli priestor medzi priesecnikami
		vykresli_medzi(ymin, priesecnik1, priesecnik2);

		//pripocitovanie priesecnika, a vykreslenie bodov medzi priesecnikom
		for (int ya = ymin + 1; ya < ymax; ya++) {
			priesecnik1.x += w1;
			priesecnik1.y = ya;

			priesecnik2.x += w2;
			priesecnik2.y = ya;

			if (trojuholnik[0].y < trojuholnik[1].y) {
				priesecnik1.color = interpolacia_farbe_podla_y(trojuholnik[0], trojuholnik[1], priesecnik1);
				priesecnik2.color = interpolacia_farbe_podla_y(trojuholnik[0], trojuholnik[2], priesecnik1);
			}
			else {
				priesecnik1.color = interpolacia_farbe_podla_y(trojuholnik[1], trojuholnik[0], priesecnik1);
				priesecnik2.color = interpolacia_farbe_podla_y(trojuholnik[2], trojuholnik[0], priesecnik1);
			}

			vykresli_medzi(ya, priesecnik1, priesecnik2);
		}
	}

	//asi to netreba
	/*
	//vykreslenie posledneho bodu alebo riadku
	//ak je trojuholnik \/ spicom dolu
	if (trojuholnik[0].y < trojuholnik[1].y) {
		//kontrola so z-bufferom a ci je mimo platna
		if (!mimo_platna(trojuholnik[0])) {
			if (Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)] > trojuholnik[0].z) {
				Z[(int)(trojuholnik[0].x + 0.5)][(int)(trojuholnik[0].y + 0.5)] = trojuholnik[0].z;
				draw_point((int)(trojuholnik[0].x + 0.5), (int)(trojuholnik[0].y + 0.5), trojuholnik[0].color);
			}
		}
	}
	//ak je trojuholnik /\ spicom hore
	else {
		//vykresli sa prvy bod dolnej priamky + z-buffer + mimo_platna
		if (!mimo_platna(trojuholnik[1])) {
			if (Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)] > trojuholnik[1].z) {
				Z[(int)(trojuholnik[1].x + 0.5)][(int)(trojuholnik[1].y + 0.5)] = trojuholnik[1].z;
				draw_point((int)(trojuholnik[1].x + 0.5), (int)(trojuholnik[1].y + 0.5), trojuholnik[1].color);
			}
		}

		//vykresli sa posledny bod dolnej priamky + z-buffer + mimo_platna
		if (!mimo_platna(trojuholnik[2])) {
			if (Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)] > trojuholnik[1].z) {
				Z[(int)(trojuholnik[2].x + 0.5)][(int)(trojuholnik[2].y + 0.5)] = trojuholnik[1].z;
				draw_point((int)(trojuholnik[1].x + 0.5), (int)(trojuholnik[1].y + 0.5), trojuholnik[2].color);
			}
		}

		//vykreslenie medzi bodami
		vykresli_medzi((int)(trojuholnik[1].y + 0.5), trojuholnik[1], trojuholnik[2]);
	}
	*/
}

void vtk_paint::vykresli_medzi(QColor vysledna_farba, int ya, suradnice odkial, suradnice pokial) {
	//vykresli body medzi odkial az pokial

	for (int xa = (int)(odkial.x + 0.5); xa <= (int)(pokial.x + 0.5); xa++) {
		//vypocitanie z suradnice (interpolacia)
		float za = odkial.z + (xa - odkial.x) * ((pokial.z - odkial.z) / (pokial.x - odkial.x));

		suradnice tmp;
		tmp.x = xa;
		tmp.y = ya;
		tmp.z = za;

		//z-buffer + vykreslenie + mimo_platna
		if (!mimo_platna(tmp)) {
			if (Z[xa][ya] > za) {
				Z[xa][ya] = za;
				draw_point(xa, ya, vysledna_farba);
			}
			//IBA KONTROLA z_buffera
			/*		QMessageBox box;
					box.setText("Z-buffer: " + QString::number(Z[xa][ya]) + "\n z-ova bod: " + QString::number(za));
					box.exec();*/
		}
	}
	//IBA KONTROLA priesecnikov
	/*		QMessageBox box;
			box.setText("Odkial: " + QString::number(odkial.x) + " " + QString::number(odkial.y) + " " + QString::number(odkial.z) + "\nPokial: " + QString::number(pokial.x) + " " + QString::number(pokial.y) + " " + QString::number(pokial.z));
			box.exec();*/
}

void vtk_paint::vykresli_medzi(int ya, suradnice odkial, suradnice pokial) {
	//vykresli body medzi odkial az pokial

	for (int xa = (int)(odkial.x + 0.5); xa <= (int)(pokial.x + 0.5); xa++) {
		//vypocitanie z suradnice (interpolacia)
		float za = odkial.z + (xa - odkial.x) * ((pokial.z - odkial.z) / (pokial.x - odkial.x));

		suradnice tmp;
		tmp.x = xa;
		tmp.y = ya;
		tmp.z = za;

		//z-buffer + vykreslenie + mimo_platna
		if (!mimo_platna(tmp)) {
			if (Z[xa][ya] > za) {
				Z[xa][ya] = za;

				QColor vysledna_farba = interpolacia_farbe_podla_x(odkial, pokial, tmp);

				draw_point(xa, ya, vysledna_farba);
			}
			//IBA KONTROLA z_buffera
			/*		QMessageBox box;
					box.setText("Z-buffer: " + QString::number(Z[xa][ya]) + "\n z-ova bod: " + QString::number(za));
					box.exec();*/
		}
	}
	//IBA KONTROLA priesecnikov	
	/*		QMessageBox box;
			box.setText("Odkial: " + QString::number(odkial.x) + " " + QString::number(odkial.y) + " " + QString::number(odkial.z) + "\nPokial: " + QString::number(pokial.x) + " " + QString::number(pokial.y) + " " + QString::number(pokial.z));
			box.exec();*/
}

QColor vtk_paint::vysledna_intenzita(suradnice normala_bodu_plosky) {
//	printf("Pocitam vyslednu intenzitu trojuholnika... \n");

	QColor vysledna_farba, ambientna_zlozka, zrkadlova_zlozka, difuzna_zlozka;
	float vektor_pohladu_odrazeny_luc, svetelny_luc_normala;
	suradnice normala, svetelny_luc, odrazeny_luc;

	normala = normovanie(normala_bodu_plosky);

	//ambientna zlozka
	//+ kontrola rangu zloziek
	ambientna_zlozka = QColor(kontrola_zlozky(ambientna.x * farba_okolie.red()), kontrola_zlozky(ambientna.y * farba_okolie.green()), kontrola_zlozky(ambientna.z * farba_okolie.blue()));

	//nacitanie ambientnej zlozky do vyslednej farbe
	vysledna_farba = QColor(ambientna_zlozka.red(), ambientna_zlozka.green(), ambientna_zlozka.blue());

	//pricitaju sa dalej veci vypoctu svetla podla toho kelko je svetelnych lucov
	for (int j = 0; j < svetelne_luce.size(); j++) {
		//normovanie svetelneho lucu
		svetelny_luc = normovanie(svetelne_luce[j]);

		//IBA KONTROLA aktualnych hodnot vektorov
		/*		printf("Normala: %f %f %f \n", normala.x, normala.y, normala.z);
				printf("Svetelny luc: %f %f %f \n", svetelne_luce[j].x, svetelne_luce[j].y, svetelne_luce[j].z);
				printf("Vektor pohladu: %f %f %f \n", vektor_pohladu.x, vektor_pohladu.y, vektor_pohladu.z);
				printf("Odrazeny luc: %f %f %f \n\n", odrazeny_luc.x, odrazeny_luc.y, odrazeny_luc.z);*/

		//nasobok normaly a svetelneho luca
		svetelny_luc_normala = normala.x * svetelny_luc.x + normala.y * svetelny_luc.y + normala.z * svetelny_luc.z;

		//vypocet odrazeneho lucu
		odrazeny_luc.x = 2 * svetelny_luc_normala * normala.x - svetelny_luc.x;
		odrazeny_luc.y = 2 * svetelny_luc_normala * normala.y - svetelny_luc.y;
		odrazeny_luc.z = 2 * svetelny_luc_normala * normala.z - svetelny_luc.z;
		
		//normovanie odrazeneho lucu
		odrazeny_luc = normovanie(odrazeny_luc);

		//nasobok vektora pohladu a odrazeneho lucu
		vektor_pohladu_odrazeny_luc = vektor_pohladu.x * odrazeny_luc.x + vektor_pohladu.y * odrazeny_luc.y + vektor_pohladu.z * odrazeny_luc.z;

		//zrkadlova zlozka bez farby zdroja
		//ak je nasobok svetelneho luca a normaly alebo vektora pohladu a odrazeneho luca <= 0, vtedy zrkadlova zlozka bude 0
		//+ kontrola rangu zloziek
		if ((svetelny_luc_normala <= 0) || (vektor_pohladu_odrazeny_luc <= 0)) {
			zrkadlova_zlozka = QColor(0, 0, 0);
		}
		else {
			zrkadlova_zlozka = QColor(kontrola_zlozky(farba_zdroj.red() * zrkadlova.x * pow(vektor_pohladu_odrazeny_luc, ostrost)), kontrola_zlozky(farba_zdroj.green() * zrkadlova.y * pow(vektor_pohladu_odrazeny_luc, ostrost)), kontrola_zlozky(farba_zdroj.blue() * zrkadlova.z * pow(vektor_pohladu_odrazeny_luc, ostrost)));
		}

		//ak je nasobok svetelneho luca a normaly <= 0, vtedy difuzna zlozka bude 0
		//+ kontrola rangu zloziek
		if (svetelny_luc_normala <= 0) {
			difuzna_zlozka = QColor(0, 0, 0);
		}
		else {
			difuzna_zlozka = QColor(kontrola_zlozky(farba_zdroj.red() * difuzia.x * svetelny_luc_normala), kontrola_zlozky(farba_zdroj.green() * difuzia.y * svetelny_luc_normala), kontrola_zlozky(farba_zdroj.blue() * difuzia.z * svetelny_luc_normala));
		}

		//pricita sa zrkadlova a difuzna zlozka
		//+ kontrola rangu zloziek
		vysledna_farba = QColor(kontrola_zlozky(vysledna_farba.red() + zrkadlova_zlozka.red() + difuzna_zlozka.red()), kontrola_zlozky(vysledna_farba.green() + zrkadlova_zlozka.green() + difuzna_zlozka.green()), kontrola_zlozky(vysledna_farba.blue() + zrkadlova_zlozka.blue() + difuzna_zlozka.blue()));
	}

	return vysledna_farba;
}

//nepouziva sa, ale ta knizka vravi, ze toto je za Gourauda
QColor vtk_paint::vysledna_intenzita_bez_zrkadlovej(suradnice normala_bodu) {
//	printf("Pocitam vyslednu intenzitu bodu... \n");

	QColor vysledna_farba, ambientna_zlozka, zrkadlova_zlozka, difuzna_zlozka;
	float vektor_pohladu_odrazeny_luc, svetelny_luc_normala;
	suradnice normala, svetelny_luc, odrazeny_luc;

	normala = normovanie(normala_bodu);

	//ambientna zlozka
	//+ kontrola rangu zloziek
	ambientna_zlozka = QColor(kontrola_zlozky(ambientna.x * farba_okolie.red()), kontrola_zlozky(ambientna.y * farba_okolie.green()), kontrola_zlozky(ambientna.z * farba_okolie.blue()));

	//nacitanie ambientnej zlozky do vyslednej farbe
	vysledna_farba = QColor(ambientna_zlozka.red(), ambientna_zlozka.green(), ambientna_zlozka.blue());

	//pricitaju sa dalej veci vypoctu svetla podla toho kelko je svetelnych lucov
	for (int j = 0; j < svetelne_luce.size(); j++) {
		//normovanie svetelneho lucu
		svetelny_luc = normovanie(svetelne_luce[j]);

		//nasobok normaly a svetelneho luca
		svetelny_luc_normala = normala.x * svetelny_luc.x + normala.y * svetelny_luc.y + normala.z * svetelny_luc.z;

		//vypocet odrazeneho lucu
		odrazeny_luc.x = 2 * svetelny_luc_normala * normala.x - svetelny_luc.x;
		odrazeny_luc.y = 2 * svetelny_luc_normala * normala.y - svetelny_luc.y;
		odrazeny_luc.z = 2 * svetelny_luc_normala * normala.z - svetelny_luc.z;

		//normovanie odrazeneho lucu
		odrazeny_luc = normovanie(odrazeny_luc);

		//nasobok vektora pohladu a odrazeneho lucu
		vektor_pohladu_odrazeny_luc = vektor_pohladu.x * odrazeny_luc.x + vektor_pohladu.y * odrazeny_luc.y + vektor_pohladu.z * odrazeny_luc.z;

		//ak je nasobok svetelneho luca a normaly <= 0, vtedy difuzna zlozka bude 0
		//+ kontrola rangu zloziek
		if (svetelny_luc_normala <= 0) {
			difuzna_zlozka = QColor(0, 0, 0);
		}
		else {
			difuzna_zlozka = QColor(kontrola_zlozky(farba_zdroj.red() * difuzia.x * svetelny_luc_normala), kontrola_zlozky(farba_zdroj.green() * difuzia.y * svetelny_luc_normala), kontrola_zlozky(farba_zdroj.blue() * difuzia.z * svetelny_luc_normala));
		}

		//pricita sa zrkadlova a difuzna zlozka
		//+ kontrola rangu zloziek
		vysledna_farba = QColor(kontrola_zlozky(vysledna_farba.red() + difuzna_zlozka.red()), kontrola_zlozky(vysledna_farba.green() + difuzna_zlozka.green()), kontrola_zlozky(vysledna_farba.blue() + difuzna_zlozka.blue()));
	}

	return vysledna_farba;
}

QColor vtk_paint::interpolacia_farbe_podla_x(suradnice prvy, suradnice druhy, suradnice aktualny) {
	QColor farba;

	farba.setRed(kontrola_zlozky(prvy.color.red() + (aktualny.x - prvy.x) * (druhy.color.red() - prvy.color.red()) / (druhy.x - prvy.x)));
	farba.setGreen(kontrola_zlozky(prvy.color.green() + (aktualny.x - prvy.x) * (druhy.color.green() - prvy.color.green()) / (druhy.x - prvy.x)));
	farba.setBlue(kontrola_zlozky(prvy.color.blue() + (aktualny.x - prvy.x) * (druhy.color.blue() - prvy.color.blue()) / (druhy.x - prvy.x)));

	return farba;
}

QColor vtk_paint::interpolacia_farbe_podla_y(suradnice prvy, suradnice druhy, suradnice aktualny) {
	QColor farba;

	farba.setRed(kontrola_zlozky(prvy.color.red() + (aktualny.y - prvy.y) * (druhy.color.red() - prvy.color.red()) / (druhy.y - prvy.y)));
	farba.setGreen(kontrola_zlozky(prvy.color.green() + (aktualny.y - prvy.y) * (druhy.color.green() - prvy.color.green()) / (druhy.y - prvy.y)));
	farba.setBlue(kontrola_zlozky(prvy.color.blue() + (aktualny.y - prvy.y) * (druhy.color.blue() - prvy.color.blue()) / (druhy.y - prvy.y)));

	return farba;
}

void vtk_paint::gouraudovo_tienovanie() {
	printf("Robim Gouraudovo tienovanie... \n");

	int sirka = image.width();
	int vyska = image.height();

	//vytvorenie dvojrozmerneho pola pre uladanie z suradnici
	Z = new float *[sirka];

	for (int i = 0; i < sirka; i++) {
		Z[i] = new float[vyska];
	}

	//naplanenie tychto poli
	for (int i = 0; i < sirka; i++) {
		for (int j = 0; j < vyska; j++) {
			Z[i][j] = (HUGE_VALF);
		}
	}

	//nastavy vector farieb na velkost polygonov
	farby_bodov.resize(polygony.size());

	//vypocita narmalu a farbu kazdeho vrcholu
	vypocet_normal_vrcholov();

	//prejde vsetky trojuholniky
	for (int i = 0; i < polygony.size(); i++) {
//	for (int i = 0; i < 4; i++){							//KED KCEME ZMENSIT POCET POLYGONOV
		std::vector <suradnice> body_trojuholnika;
		suradnice tmp;

		//prvy bod trojuhonika
		tmp.x = body_kresli[polygony[i].prvy].x;
		tmp.y = body_kresli[polygony[i].prvy].y;
		tmp.z = body_prepocitane[polygony[i].prvy].z;
		tmp.color = farby_bodov[polygony[i].prvy];
		body_trojuholnika.push_back(tmp);

		//druhy bod trojuholnika
		tmp.x = body_kresli[polygony[i].druhy].x;
		tmp.y = body_kresli[polygony[i].druhy].y;
		tmp.z = body_prepocitane[polygony[i].druhy].z;
		tmp.color = farby_bodov[polygony[i].druhy];
		body_trojuholnika.push_back(tmp);

		//treti bod trojuholnika
		tmp.x = body_kresli[polygony[i].treti].x;
		tmp.y = body_kresli[polygony[i].treti].y;
		tmp.z = body_prepocitane[polygony[i].treti].z;
		tmp.color = farby_bodov[polygony[i].treti];
		body_trojuholnika.push_back(tmp);

		//vysortovanie podla y body_trojuholnika
		qSort(body_trojuholnika.begin(), body_trojuholnika.end(), sort_podla_y);

		//IBA KONTROLA povodbe body
		/*	printf("Povodne body-trojuhonika: \n");
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[0].x, body_trojuholnika[0].y, body_trojuholnika[0].z);
			printf("R: %d, G: %d, B: %d \n", body_trojuholnika[0].color.red(), body_trojuholnika[0].color.green(), body_trojuholnika[0].color.blue());
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[1].x, body_trojuholnika[1].y, body_trojuholnika[1].z);
			printf("R: %d, G: %d, B: %d \n", body_trojuholnika[1].color.red(), body_trojuholnika[1].color.green(), body_trojuholnika[1].color.blue());
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[2].x, body_trojuholnika[2].y, body_trojuholnika[2].z);
			printf("R: %d, G: %d, B: %d \n", body_trojuholnika[2].color.red(), body_trojuholnika[2].color.green(), body_trojuholnika[2].color.blue());
			getchar();*/

		//ak sa dva body nerovnake (1. a 2. aa 2. a 3.)
		if ((body_trojuholnika[0].y != body_trojuholnika[1].y) && (body_trojuholnika[1].y != body_trojuholnika[2].y)) {
			//vytvori sa novy bod a prida sa do body_trojuholnika
			tmp.y = body_trojuholnika[1].y;
			tmp.x = body_trojuholnika[0].x + (tmp.y - body_trojuholnika[0].y) * ((body_trojuholnika[2].x - body_trojuholnika[0].x) / (body_trojuholnika[2].y - body_trojuholnika[0].y));
			tmp.z = body_trojuholnika[0].z + (tmp.y - body_trojuholnika[0].y) * ((body_trojuholnika[2].z - body_trojuholnika[0].z) / (body_trojuholnika[2].y - body_trojuholnika[0].y));
			tmp.color = interpolacia_farbe_podla_y(body_trojuholnika[0], body_trojuholnika[2], tmp);
			body_trojuholnika.push_back(tmp);

			//vysortovanie podla y body_trojuholnika
			qSort(body_trojuholnika.begin(), body_trojuholnika.end(), sort_podla_y);
			
			//IBA KONTROLA, ked sa prida bod
			/*	printf("Body-trojuhonika s pridanym bodom: \n");
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[0].x, body_trojuholnika[0].y, body_trojuholnika[0].z);
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[1].x, body_trojuholnika[1].y, body_trojuholnika[1].z);
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[2].x, body_trojuholnika[2].y, body_trojuholnika[2].z);
			printf("X: %f, Y: %f, Z: %f \n", body_trojuholnika[3].x, body_trojuholnika[3].y, body_trojuholnika[3].z);
			getchar();*/

			//POMOCNE vykreslenie bodu tmp
			/*if (!mimo_platna(tmp)) {
				printf("Pred: %f \n", Z[(int)(tmp.x + 0.5)][(int)(tmp.y + 0.5)]);
				if (Z[(int)(tmp.x + 0.5)][(int)(tmp.y + 0.5)] > tmp.z) {
					Z[(int)(tmp.x + 0.5)][(int)(tmp.y + 0.5)] = tmp.z;
				printf("Po: %f \n", Z[(int)(tmp.x + 0.5)][(int)(tmp.y + 0.5)]);
					draw_point((int)(tmp.x + 0.5), (int)(tmp.y + 0.5), tmp.color);
							QMessageBox box;
							box.setText("Z-buffer: " + QString::number(Z[(int)(tmp.x + 0.5)][(int)(tmp.y + 0.5)]) + "\n > z-ova bod: " + QString::number(tmp.z));
							printf("R: %d, G: %d, B: %d \n", tmp.color.red(), tmp.color.green(), tmp.color.blue());
							box.exec();
				}
				printf("Celkom po: %f \n", Z[(int)(tmp.x + 0.5)][(int)(tmp.y + 0.5)]);
			}*/

			std::vector <suradnice> horny_trojuholnik;
			horny_trojuholnik.resize(3);

			//nacitanie bodov horneho trojuholnika
			for (int j = 0; j < body_trojuholnika.size() - 1; j++) {
				horny_trojuholnik[j].x = body_trojuholnika[j].x;
				horny_trojuholnik[j].y = body_trojuholnika[j].y;
				horny_trojuholnik[j].z = body_trojuholnika[j].z;
				horny_trojuholnik[j].color = body_trojuholnika[j].color;
			}

			std::vector <suradnice> dolny_trojuholnik;
			dolny_trojuholnik.resize(3);

			//nacitanie bodov dolneho trojuhonika
			for (int j = 1; j < body_trojuholnika.size(); j++) {
				dolny_trojuholnik[j - 1].x = body_trojuholnika[j].x;
				dolny_trojuholnik[j - 1].y = body_trojuholnika[j].y;
				dolny_trojuholnik[j - 1].z = body_trojuholnika[j].z;
				dolny_trojuholnik[j - 1].color = body_trojuholnika[j].color;
			}

			//vyplnenie dolneho trojuholnika
			vypln(dolny_trojuholnik);

			//vyplnenie horneho trojuholnika
			vypln(horny_trojuholnik);
		}
		//ak sa rovna y bodu 1 a bodu 2 ALEBO ak sa rovna y bodu 2 a bodu 3
		//vytvori sa iba jeden trojuholnik, tj. nevytvori sa nist, lebo uz existuje ako body_trojuholnika
		else {
			//vyplnenie toho jedneho trojuholnika
			vypln(body_trojuholnika);
		}
	}

	//dealokacia poli
	for (int i = 0; i < image.height(); i++) {
		delete[] Z[i];
	}

	delete[] Z;
}

suradnice vtk_paint::vypocet_normaly(int i) {
	suradnice normala;
	suradnice prvy_vektor;
	suradnice druhy_vektor;

	//vypocet trveho vektora v trojuhoniku
	prvy_vektor.x = body_original[polygony[i].druhy].x - body_original[polygony[i].prvy].x;
	prvy_vektor.y = body_original[polygony[i].druhy].y - body_original[polygony[i].prvy].y;
	prvy_vektor.z = body_original[polygony[i].druhy].z - body_original[polygony[i].prvy].z;

	//vypocet druheho vektora v trojuholniku
	druhy_vektor.x = body_original[polygony[i].treti].x - body_original[polygony[i].prvy].x;
	druhy_vektor.y = body_original[polygony[i].treti].y - body_original[polygony[i].prvy].y;
	druhy_vektor.z = body_original[polygony[i].treti].z - body_original[polygony[i].prvy].z;

	//vypocet normaly
	normala.x = prvy_vektor.y * druhy_vektor.z - prvy_vektor.z * druhy_vektor.y;
	normala.y = prvy_vektor.z * druhy_vektor.x - prvy_vektor.x * druhy_vektor.z;
	normala.z = prvy_vektor.x * druhy_vektor.y - prvy_vektor.y * druhy_vektor.x;

	return normala;
}

suradnice vtk_paint::normovanie(suradnice normovat) {
	suradnice normovany;

	//normovanie
	float n = normovat.x * normovat.x + normovat.y * normovat.y + normovat.z * normovat.z;
	n = sqrt(n);
	normovany.x = normovat.x / n;
	normovany.y = normovat.y / n;
	normovany.z = normovat.z / n;

	return normovany;
}

void vtk_paint::vypocet_normal_vrcholov() {
	//vypocita normali vo vsetkych vrcholoch a priradi im farbu

	std::vector <suradnice> normaly_trojuholnikov;
	suradnice normala_bodu;

	//prejde vsetky body po indexoch
	for (int index_bodu = 0; index_bodu < body_original.size(); index_bodu++) {
		//prejde vsetky polygony a zapamata si normalu tich, ktore maju tento bod
		for (int i = 0; i < polygony.size(); i++) {
			//ak je bod sucastou trojuholnika tak sa pocita normala
			if (polygony[i].prvy == index_bodu || polygony[i].druhy == index_bodu || polygony[i].treti == index_bodu) {
				suradnice normala_trojuholnika_tmp = vypocet_normaly(i);

				normaly_trojuholnikov.push_back(normala_trojuholnika_tmp);
			}
		}

		normala_bodu.x = 0;
		normala_bodu.y = 0;
		normala_bodu.z = 0;

		//prejde vsetky normaly trojuholnikov s danym bodom a vypocita normalu daneho bodu
		for (int i = 0; i < normaly_trojuholnikov.size(); i++) {
			normala_bodu.x += normaly_trojuholnikov[i].x;
			normala_bodu.y += normaly_trojuholnikov[i].y;
			normala_bodu.z += normaly_trojuholnikov[i].z;
		}

		//vypocita farbu v danom bode
		farby_bodov[index_bodu] = vysledna_intenzita(normala_bodu);

		//ak splna podmienky Z-buffera + mimo_platna vykresli sa bod
		if (!mimo_platna(body_kresli[index_bodu])) {
			if (Z[(int)(body_kresli[index_bodu].x + 0.5)][(int)(body_kresli[index_bodu].y + 0.5)] > body_kresli[index_bodu].z) {
				Z[(int)(body_kresli[index_bodu].x + 0.5)][(int)(body_kresli[index_bodu].y + 0.5)] = body_kresli[index_bodu].z;
				draw_point((int)(body_kresli[index_bodu].x + 0.5), (int)(body_kresli[index_bodu].y + 0.5), farby_bodov[index_bodu]);
			}
		}

		normaly_trojuholnikov.clear();
	}
}

bool vtk_paint::mimo_platna(suradnice bod) {
	//skontroluje ci je bod v platne alebo mimo
	//ak je mimo -> true
	if (bod.x < 0 || bod.x >= image.width()) {
		return true;
	}

	if (bod.y < 0 || bod.y >= image.height()) {
		return true;
	}

	//ak je ni mimo -> false
	return false;
}

int vtk_paint::kontrola_zlozky(float zlozka) {
	//skontroluje ci je farba ni mimo rang (0 - 255)
	//a pretipuje na int
	int zlozka_int;

	if (zlozka < 0.0) {
		zlozka_int = 0;
	}
	else {
		if (zlozka > 255.0) {
			zlozka_int = 255;
		}
		else {
			zlozka_int = (int)(zlozka + 0.5);
		}
	}

	return zlozka_int;
}

