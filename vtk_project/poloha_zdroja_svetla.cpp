﻿#include "poloha_zdroja_svetla.h"

poloha_zdroja_svetla::poloha_zdroja_svetla(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);


}

poloha_zdroja_svetla::~poloha_zdroja_svetla() {
	
}

void poloha_zdroja_svetla::OK_click() {
	//nacita hodnoty s ui
	QString x_string = ui.lineEdit->text();
	QString y_string = ui.lineEdit_2->text();
	QString z_string = ui.lineEdit_3->text();

	x = x_string.toInt();
	y = y_string.toInt();
	z = z_string.toInt();

	close();
}

int poloha_zdroja_svetla::povedz_x() {
	return x;
}

int poloha_zdroja_svetla::povedz_y() {
	return y;
}

int poloha_zdroja_svetla::povedz_z() {
	return z;
}