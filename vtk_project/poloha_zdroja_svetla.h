﻿#pragma once
#include <QDialog>
#include "ui_poloha_zdroja_svetla.h"

class poloha_zdroja_svetla : public QDialog {
	Q_OBJECT

public:
	poloha_zdroja_svetla(QWidget * parent = Q_NULLPTR);
	~poloha_zdroja_svetla();

	int povedz_x();
	int povedz_y();
	int povedz_z();

public slots:
	void OK_click();

private:
	Ui::poloha_zdroja_svetla ui;

	int x;
	int y;
	int z;
};
