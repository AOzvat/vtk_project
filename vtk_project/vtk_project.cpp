#include "vtk_project.h"

vtk_project::vtk_project(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::vtk_projectClass)
{
	vtkPaint = new vtk_paint(ui);
	ui->setupUi(this);

	//nastavime scroll areu
	ui->scrollArea->setWidget(this->vtkPaint);
	ui->scrollArea->setBackgroundRole(QPalette::Dark);

	//novy obrazok
	vtkPaint->new_image(800, 600);

	//signaly na posuvanie slidera
	connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(zmena_azimut(int)));
	connect(ui->horizontalSlider_2, SIGNAL(valueChanged(int)), this, SLOT(zmena_zenit(int)));
	connect(ui->horizontalSlider_3, SIGNAL(valueChanged(int)), this, SLOT(rotacia(int)));
}

vtk_project::~vtk_project()
{

}

void vtk_project::help_generovanie() {
	//sprava pre zaciatok
	QString navod_text;

	//otvorenie suboru
	QFile navod_file("Navod_generovanie.txt");
	if (navod_file.open(QFile::ReadOnly)) {
		QTextStream navod(&navod_file);
		while (!navod.atEnd()) {
			navod_text = navod_text + "\n" + navod.readLine();
		}
		navod_file.close();
	}

	QMessageBox box;
	box.setText(navod_text);
	box.exec();
}

void vtk_project::help_premietanie() {
	//sprava pre zaciatok
	QString navod_text;

	//otvorenie suboru
	QFile navod_file("Navod_premietanie.txt");
	if (navod_file.open(QFile::ReadOnly)) {
		QTextStream navod(&navod_file);
		while (!navod.atEnd()) {
			navod_text = navod_text + "\n" + navod.readLine();
		}
		navod_file.close();
	}

	QMessageBox box;
	box.setText(navod_text);
	box.exec();
}

void vtk_project::help_osvetlenie() {
	//sprava pre zaciatok
	QString navod_text;

	//otvorenie suboru
	QFile navod_file("Navod_osvetlenie.txt");
	if (navod_file.open(QFile::ReadOnly)) {
		QTextStream navod(&navod_file);
		while (!navod.atEnd()) {
			navod_text = navod_text + "\n" + navod.readLine();
		}
		navod_file.close();
	}

	QMessageBox box;
	box.setText(navod_text);
	box.exec();
}

void vtk_project::new_img() {
	vtkPaint->new_image(800, 600);
}

void vtk_project::open() {
	std::vector<suradnice> body;
	std::vector<body_poly> polygony;

	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "vtk files (*.vtk)");

	if (fileName.isEmpty()) {
		return;
	}

	QFile vtk_file(fileName);

	//test otvorenia suboru
	if (!vtk_file.open(QFile::ReadOnly)) {
		return;
	}

	QTextStream file(&vtk_file);

	for (int i = 0; i < 4; i++) {
		file.readLine();
	}

	QString line = file.readLine();

	//splitne line a dostane pocet bodov
	QStringList tmp = line.split(" ");
	int pocet_bodov = tmp[1].toInt();

	body.resize(pocet_bodov);

	//nacita body
	for (int i = 0; i < pocet_bodov; i++) {
		line = file.readLine();
		QStringList suradnice_list = line.split(" ");

		body[i].x = suradnice_list[0].toFloat();
		body[i].y = suradnice_list[1].toFloat();
		body[i].z = suradnice_list[2].toFloat();

		/*
		printf("X: %f ", body[i].x);
		printf("Y: %f ", body[i].y);
		printf("Z: %f \n", body[i].z);
		*/
	}

	line = file.readLine();

	//splitne line a dostane pocet polygonov
	tmp = line.split(" ");
	int pocet_poly = tmp[1].toInt();

	polygony.resize(pocet_poly);

	//nacita polygony
	for (int i = 0; i < pocet_poly; i++){
		line = file.readLine();
		QStringList suradnice_list = line.split(" ");

		polygony[i].prvy = suradnice_list[1].toInt();
		polygony[i].druhy = suradnice_list[2].toInt();
		polygony[i].treti = suradnice_list[3].toInt();

		/*
		printf("PRVY: %d \n", polygony[i].prvy);
		printf("DRUHY: %d \n", polygony[i].druhy);
		printf("TRETI: %d \n", polygony[i].treti);
		*/
	}

	//zatvori subor
	vtk_file.close();

	//posle sa vector bodov a polygonov
	vtkPaint->prirad_body_poly(body, polygony, ui->lineEdit->text().toFloat());

	QMessageBox mbox;
	mbox.setText("Subor bol nacitany.");
	mbox.exec();
}

void vtk_project::vymaz_click() {
	vtkPaint->vymaz_vsetko();

	//nastavy slideri na zaciatok
	ui->horizontalSlider->setValue(0);
	ui->horizontalSlider_2->setValue(0);
	ui->horizontalSlider_3->setValue(0);
}

void vtk_project::povodne_hodnoty_click() {
	//nastavit ui na default hodnoty
	ui->lineEdit->setText("300");
	ui->lineEdit_2->setText("5");
	ui->lineEdit_3->setText("5");
	ui->lineEdit_4->setText("10");
	ui->lineEdit_5->setText("5");
	ui->lineEdit_6->setText("255");
	ui->lineEdit_7->setText("0");
	ui->lineEdit_8->setText("0");
	ui->lineEdit_9->setText("255");
	ui->lineEdit_10->setText("255");
	ui->lineEdit_11->setText("255");
	ui->lineEdit_12->setText("0.2");
	ui->lineEdit_13->setText("0.2");
	ui->lineEdit_14->setText("0.2");
	ui->lineEdit_15->setText("5");
	ui->lineEdit_16->setText("100");
	ui->lineEdit_17->setText("10");
	ui->lineEdit_18->setText("10");
	ui->lineEdit_19->setText("0");
	ui->lineEdit_20->setText("0");
	ui->lineEdit_21->setText("0.2");
	ui->lineEdit_22->setText("0.2");
	ui->lineEdit_23->setText("0.2");
	ui->lineEdit_24->setText("0.2");

	ui->radioButton->setChecked(false);
	ui->radioButton_2->setChecked(true);
	ui->radioButton_3->setChecked(true);
	ui->radioButton_4->setChecked(false);

	ui->comboBox_2->setCurrentIndex(1);
	ui->comboBox_3->setCurrentIndex(1);

	ui->spinBox->setValue(1);

	svetelne_luce.clear();
}

void vtk_project::kresli_click() {
	//vymaze kresliacu plochu
	vtkPaint->clear_image();
	
	//ak je vybrane rovnobezne premietanie
	if (ui->comboBox_2->currentIndex() == 0) {
		vtkPaint->rovnobezne();
	}

	//ak je vybrane stredove premietanie
	if (ui->comboBox_2->currentIndex() == 1) {
		vtkPaint->stredove(ui->lineEdit->text().toFloat());
	}

	//ak je zaskrtnute osvetlovanie
	if (ui->radioButton_4->isChecked()) {
		//premaze plochu, aby tam nebol wireframe
		vtkPaint->clear_image();

		//ak je svetelny luc nenastaveky
		if (svetelne_luce.size() == 0) {
			for (int i = 0; i < ui->spinBox->value(); i++) {
				//nastavy hodnoty dafault
				suradnice tmp;
				tmp.x = 0;
				tmp.y = 0;
				tmp.z = 10000.0;

				svetelne_luce.push_back(tmp);
			}
		}

		//nacita s ui veci pre osvetlenie a posle ich do vtk_paint
		veci_na_osvetlenie();

		//ak je vybrane konstantne tienovanie
		if (ui->comboBox_3->currentIndex() == 0) {
			vtkPaint->konstantne_tienovanie();
		}

		//ak je vybrane Gouraudovo tienovanie
		if (ui->comboBox_3->currentIndex() == 1) {
			vtkPaint->gouraudovo_tienovanie();
		}
	}

	//KONTROLA
	/*
	vtkPaint->rovnobezne();
	*/

	printf("Hotovo! \n");
}

void vtk_project::generuj_click() {
	//generuje vtk_file

	//generujeme elipsoid
	if (ui->radioButton_2->isChecked()) {
		vtkPaint->generuj_elipsoid(ui->lineEdit_5->text().toFloat(), ui->lineEdit_4->text().toFloat(), ui->lineEdit_3->text().toInt(), ui->lineEdit_2->text().toInt());
	}

	//generuje sa Bezierova krivka
	if (ui->radioButton->isChecked()) {
		vtkPaint->generuj_bezier(ui->lineEdit_16->text().toInt(), ui->lineEdit_17->text().toInt(), ui->lineEdit_18->text().toInt());
	}

	//vymaze kresliacu plochu
	vtkPaint->clear_image();
}

void vtk_project::zadaj_polohu_svetla_click() {
	//nastavy sa poloha svetiel

	//najprv premaze vektor polohy
	svetelne_luce.clear();

	for (int i = 0; i < ui->spinBox->value(); i++) {
		poloha_zdroja_svetla poloha;
		poloha.exec();

		//precita nastavene hodnoty
		suradnice tmp;
		tmp.x = (float)(poloha.povedz_x());
		tmp.y = (float)(poloha.povedz_y());
		tmp.z = (float)(poloha.povedz_z());

		svetelne_luce.push_back(tmp);
	}
}

void vtk_project::zmena_azimut(int uhol) {
	//prime signal zo slidera azimutu

	//vymaze kresliacu plochu
	vtkPaint->clear_image();

	//nastavy azimut na hodnotu
	posli_azimut = uhol;

	//ak je vybrane rovnobezne premietanie
	if (ui->comboBox_2->currentIndex() == 0) {
		vtkPaint->prepocitaj_azimut_zenit(posli_azimut, posli_zenit);
	}

	//ak je vybrane stredove premietanie
	if (ui->comboBox_2->currentIndex() == 1) {
		vtkPaint->prepocitaj_azimut_zenit(posli_azimut, posli_zenit, false, ui->lineEdit->text().toFloat());
	}
}

void vtk_project::zmena_zenit(int uhol) {
	//prime signal zo slidera zenitu

	//vymaze kresliacu plochu
	vtkPaint->clear_image();

	//nastavy zenit na hodnotu
	posli_zenit = uhol;

	//ak je vybrane rovnobezne premietanie
	if (ui->comboBox_2->currentIndex() == 0) {
		vtkPaint->prepocitaj_azimut_zenit(posli_azimut, posli_zenit);
	}

	//ak je vybrane stredove premietanie
	if (ui->comboBox_2->currentIndex() == 1) {
		vtkPaint->prepocitaj_azimut_zenit(posli_azimut, posli_zenit, false, ui->lineEdit->text().toFloat());
	}
}

void vtk_project::rotacia(int uhol) {
	//prime signal zo slidera rotacii

	//vymaze kresliacu plochu
	vtkPaint->clear_image();

	//-uhol, aby sa tocilo najprv v smere proti rucickam na hodinach
	//ak je vybrane rovnobezne premietanie
	if (ui->comboBox_2->currentIndex() == 0) {
		vtkPaint->prirad_uhol_rotacii(-uhol);		
	}

	//ak je vybrane stredove premietanie
	if (ui->comboBox_2->currentIndex() == 1) {
		vtkPaint->prirad_uhol_rotacii(-uhol, false, ui->lineEdit->text().toFloat());
	}
}

void vtk_project::veci_na_osvetlenie() {
	//nacita s ui veci pre osvetlenie
	//farba
	QColor farba_zdroj(ui->lineEdit_6->text().toInt(), ui->lineEdit_7->text().toInt(), ui->lineEdit_8->text().toInt());
	QColor farba_okolie(ui->lineEdit_9->text().toInt(), ui->lineEdit_10->text().toInt(), ui->lineEdit_11->text().toInt());

	//difuzia
	suradnice difuzia;
	difuzia.x = ui->lineEdit_12->text().toFloat();
	difuzia.y = ui->lineEdit_19->text().toFloat();
	difuzia.z = ui->lineEdit_20->text().toFloat();

	//zrkadlova
	suradnice zrkadlova;
	zrkadlova.x = ui->lineEdit_13->text().toFloat();
	zrkadlova.y = ui->lineEdit_21->text().toFloat();
	zrkadlova.z = ui->lineEdit_22->text().toFloat();

	int ostrost;
	ostrost = ui->lineEdit_15->text().toInt();

	//ambientna
	suradnice ambientna;
	ambientna.x = ui->lineEdit_14->text().toFloat();
	ambientna.y = ui->lineEdit_24->text().toFloat();
	ambientna.z = ui->lineEdit_23->text().toFloat();


	//posle ich do vtk_paint
	vtkPaint->nacitaj_osvetlovacie_veci(svetelne_luce, farba_zdroj, farba_okolie, difuzia, zrkadlova, ostrost, ambientna);
}

