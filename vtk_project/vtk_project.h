#ifndef VTK_PROJECT_H
#define VTK_PROJECT_H

#include <QtWidgets/QMainWindow>
#include "ui_vtk_project.h"
#include "vtk_paint.h"
#include <QMessageBox>
#include "poloha_zdroja_svetla.h"

class vtk_project : public QMainWindow
{
	Q_OBJECT

public:
	vtk_project(QWidget *parent = 0);
	~vtk_project();

public slots:
	void help_generovanie();
	void help_premietanie();
	void help_osvetlenie();

	void new_img();
	void open();

	void vymaz_click();
	void povodne_hodnoty_click();
	void kresli_click();
	void generuj_click();
	void zadaj_polohu_svetla_click();

	void zmena_azimut(int uhol);
	void zmena_zenit(int uhol);

	void rotacia(int uhol);

	void veci_na_osvetlenie();

private:
	Ui::vtk_projectClass *ui;
	vtk_paint *vtkPaint;
	int posli_azimut;
	int posli_zenit;

	std::vector<suradnice> svetelne_luce;
};

#endif // VTK_PROJECT_H
