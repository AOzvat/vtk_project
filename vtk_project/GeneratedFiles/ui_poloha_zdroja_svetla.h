/********************************************************************************
** Form generated from reading UI file 'poloha_zdroja_svetla.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_POLOHA_ZDROJA_SVETLA_H
#define UI_POLOHA_ZDROJA_SVETLA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_poloha_zdroja_svetla
{
public:
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLabel *label_3;
    QLineEdit *lineEdit_2;
    QLabel *label_4;
    QLineEdit *lineEdit_3;
    QLabel *label_6;
    QPushButton *pushButton;
    QLabel *label_5;

    void setupUi(QDialog *poloha_zdroja_svetla)
    {
        if (poloha_zdroja_svetla->objectName().isEmpty())
            poloha_zdroja_svetla->setObjectName(QStringLiteral("poloha_zdroja_svetla"));
        poloha_zdroja_svetla->resize(331, 191);
        formLayoutWidget = new QWidget(poloha_zdroja_svetla);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 10, 308, 169));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setPointSize(10);
        label->setFont(font);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        lineEdit = new QLineEdit(formLayoutWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setFont(font);

        formLayout->setWidget(2, QFormLayout::FieldRole, lineEdit);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        lineEdit_2 = new QLineEdit(formLayoutWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setFont(font);

        formLayout->setWidget(3, QFormLayout::FieldRole, lineEdit_2);

        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font);

        formLayout->setWidget(4, QFormLayout::LabelRole, label_4);

        lineEdit_3 = new QLineEdit(formLayoutWidget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setFont(font);

        formLayout->setWidget(4, QFormLayout::FieldRole, lineEdit_3);

        label_6 = new QLabel(formLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(5, QFormLayout::FieldRole, label_6);

        pushButton = new QPushButton(formLayoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout->setWidget(6, QFormLayout::FieldRole, pushButton);

        label_5 = new QLabel(formLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_5);


        retranslateUi(poloha_zdroja_svetla);
        QObject::connect(pushButton, SIGNAL(clicked()), poloha_zdroja_svetla, SLOT(OK_click()));

        QMetaObject::connectSlotsByName(poloha_zdroja_svetla);
    } // setupUi

    void retranslateUi(QDialog *poloha_zdroja_svetla)
    {
        poloha_zdroja_svetla->setWindowTitle(QApplication::translate("poloha_zdroja_svetla", "poloha_zdroja_svetla", 0));
        label->setText(QApplication::translate("poloha_zdroja_svetla", "Zadajte polohu zdroja svetla:", 0));
        label_2->setText(QApplication::translate("poloha_zdroja_svetla", "X - suradnica:", 0));
        lineEdit->setText(QApplication::translate("poloha_zdroja_svetla", "0", 0));
        label_3->setText(QApplication::translate("poloha_zdroja_svetla", "Y - suradnica", 0));
        lineEdit_2->setText(QApplication::translate("poloha_zdroja_svetla", "0", 0));
        label_4->setText(QApplication::translate("poloha_zdroja_svetla", "Z - suradnica", 0));
        lineEdit_3->setText(QApplication::translate("poloha_zdroja_svetla", "10000", 0));
        label_6->setText(QString());
        pushButton->setText(QApplication::translate("poloha_zdroja_svetla", "OK", 0));
        label_5->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class poloha_zdroja_svetla: public Ui_poloha_zdroja_svetla {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_POLOHA_ZDROJA_SVETLA_H
