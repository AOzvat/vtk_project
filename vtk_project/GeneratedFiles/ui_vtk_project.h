/********************************************************************************
** Form generated from reading UI file 'vtk_project.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VTK_PROJECT_H
#define UI_VTK_PROJECT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_vtk_projectClass
{
public:
    QAction *actionHelp;
    QAction *actionNew;
    QAction *actionOpen;
    QAction *actionGenerovanie;
    QAction *actionPremietanie;
    QAction *actionOsvetlenie;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QTabWidget *tabWidget;
    QWidget *tab;
    QFormLayout *formLayout_2;
    QLabel *label_4;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton;
    QSpacerItem *verticalSpacer_6;
    QLabel *label_21;
    QLabel *label_10;
    QLineEdit *lineEdit_5;
    QLabel *label_9;
    QLineEdit *lineEdit_4;
    QSpacerItem *verticalSpacer_4;
    QLabel *label_12;
    QLineEdit *lineEdit_3;
    QLabel *label_13;
    QLineEdit *lineEdit_2;
    QSpacerItem *verticalSpacer_5;
    QLabel *label_7;
    QLabel *label_27;
    QLineEdit *lineEdit_16;
    QLabel *label_30;
    QLineEdit *lineEdit_17;
    QLabel *label_32;
    QLineEdit *lineEdit_18;
    QSpacerItem *verticalSpacer_8;
    QPushButton *pushButton_3;
    QWidget *tab_3;
    QFormLayout *formLayout_4;
    QLabel *label_2;
    QSlider *horizontalSlider;
    QLabel *label_3;
    QSlider *horizontalSlider_2;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_23;
    QLabel *label_31;
    QSlider *horizontalSlider_3;
    QSpacerItem *verticalSpacer_14;
    QLabel *label_22;
    QComboBox *comboBox_2;
    QLabel *label_8;
    QLineEdit *lineEdit;
    QLabel *label_6;
    QSpacerItem *verticalSpacer;
    QLabel *label_33;
    QWidget *tab_4;
    QFormLayout *formLayout_5;
    QLabel *label;
    QSpinBox *spinBox;
    QLabel *label_5;
    QPushButton *pushButton_4;
    QSpacerItem *verticalSpacer_10;
    QLabel *label_11;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_16;
    QHBoxLayout *horizontalLayout_9;
    QLineEdit *lineEdit_6;
    QLineEdit *lineEdit_7;
    QLineEdit *lineEdit_8;
    QSpacerItem *verticalSpacer_13;
    QLabel *label_17;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *label_20;
    QHBoxLayout *horizontalLayout_11;
    QLineEdit *lineEdit_9;
    QLineEdit *lineEdit_10;
    QLineEdit *lineEdit_11;
    QSpacerItem *verticalSpacer_11;
    QLabel *label_24;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit_12;
    QLineEdit *lineEdit_19;
    QLineEdit *lineEdit_20;
    QLabel *label_25;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *lineEdit_13;
    QLineEdit *lineEdit_21;
    QLineEdit *lineEdit_22;
    QLabel *label_29;
    QLabel *label_26;
    QHBoxLayout *horizontalLayout_5;
    QLineEdit *lineEdit_14;
    QLineEdit *lineEdit_24;
    QLineEdit *lineEdit_23;
    QSpacerItem *verticalSpacer_12;
    QLabel *label_28;
    QComboBox *comboBox_3;
    QSpacerItem *verticalSpacer_9;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_3;
    QLineEdit *lineEdit_15;
    QSpacerItem *verticalSpacer_7;
    QPushButton *pushButton;
    QPushButton *pushButton_5;
    QPushButton *pushButton_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_2;
    QMenuBar *menuBar;
    QMenu *menuHelp;
    QMenu *menuHelp_2;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *vtk_projectClass)
    {
        if (vtk_projectClass->objectName().isEmpty())
            vtk_projectClass->setObjectName(QStringLiteral("vtk_projectClass"));
        vtk_projectClass->resize(1172, 715);
        actionHelp = new QAction(vtk_projectClass);
        actionHelp->setObjectName(QStringLiteral("actionHelp"));
        actionNew = new QAction(vtk_projectClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionOpen = new QAction(vtk_projectClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionGenerovanie = new QAction(vtk_projectClass);
        actionGenerovanie->setObjectName(QStringLiteral("actionGenerovanie"));
        actionPremietanie = new QAction(vtk_projectClass);
        actionPremietanie->setObjectName(QStringLiteral("actionPremietanie"));
        actionOsvetlenie = new QAction(vtk_projectClass);
        actionOsvetlenie->setObjectName(QStringLiteral("actionOsvetlenie"));
        centralWidget = new QWidget(vtk_projectClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_3 = new QVBoxLayout(centralWidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_3->setContentsMargins(0, -1, -1, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(10, 0, -1, -1);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, -1, -1);
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(10);
        tabWidget->setFont(font);
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        formLayout_2 = new QFormLayout(tab);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label_4 = new QLabel(tab);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout_2->setWidget(1, QFormLayout::SpanningRole, label_4);

        radioButton_2 = new QRadioButton(tab);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setChecked(true);

        formLayout_2->setWidget(2, QFormLayout::LabelRole, radioButton_2);

        radioButton = new QRadioButton(tab);
        radioButton->setObjectName(QStringLiteral("radioButton"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, radioButton);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        formLayout_2->setItem(4, QFormLayout::LabelRole, verticalSpacer_6);

        label_21 = new QLabel(tab);
        label_21->setObjectName(QStringLiteral("label_21"));

        formLayout_2->setWidget(5, QFormLayout::SpanningRole, label_21);

        label_10 = new QLabel(tab);
        label_10->setObjectName(QStringLiteral("label_10"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_10->sizePolicy().hasHeightForWidth());
        label_10->setSizePolicy(sizePolicy1);

        formLayout_2->setWidget(6, QFormLayout::LabelRole, label_10);

        lineEdit_5 = new QLineEdit(tab);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        sizePolicy.setHeightForWidth(lineEdit_5->sizePolicy().hasHeightForWidth());
        lineEdit_5->setSizePolicy(sizePolicy);

        formLayout_2->setWidget(6, QFormLayout::FieldRole, lineEdit_5);

        label_9 = new QLabel(tab);
        label_9->setObjectName(QStringLiteral("label_9"));
        sizePolicy1.setHeightForWidth(label_9->sizePolicy().hasHeightForWidth());
        label_9->setSizePolicy(sizePolicy1);

        formLayout_2->setWidget(7, QFormLayout::LabelRole, label_9);

        lineEdit_4 = new QLineEdit(tab);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        sizePolicy.setHeightForWidth(lineEdit_4->sizePolicy().hasHeightForWidth());
        lineEdit_4->setSizePolicy(sizePolicy);

        formLayout_2->setWidget(7, QFormLayout::FieldRole, lineEdit_4);

        verticalSpacer_4 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        formLayout_2->setItem(8, QFormLayout::LabelRole, verticalSpacer_4);

        label_12 = new QLabel(tab);
        label_12->setObjectName(QStringLiteral("label_12"));
        sizePolicy1.setHeightForWidth(label_12->sizePolicy().hasHeightForWidth());
        label_12->setSizePolicy(sizePolicy1);

        formLayout_2->setWidget(9, QFormLayout::LabelRole, label_12);

        lineEdit_3 = new QLineEdit(tab);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        sizePolicy.setHeightForWidth(lineEdit_3->sizePolicy().hasHeightForWidth());
        lineEdit_3->setSizePolicy(sizePolicy);

        formLayout_2->setWidget(9, QFormLayout::FieldRole, lineEdit_3);

        label_13 = new QLabel(tab);
        label_13->setObjectName(QStringLiteral("label_13"));
        sizePolicy1.setHeightForWidth(label_13->sizePolicy().hasHeightForWidth());
        label_13->setSizePolicy(sizePolicy1);

        formLayout_2->setWidget(10, QFormLayout::LabelRole, label_13);

        lineEdit_2 = new QLineEdit(tab);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        sizePolicy.setHeightForWidth(lineEdit_2->sizePolicy().hasHeightForWidth());
        lineEdit_2->setSizePolicy(sizePolicy);

        formLayout_2->setWidget(10, QFormLayout::FieldRole, lineEdit_2);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_2->setItem(11, QFormLayout::LabelRole, verticalSpacer_5);

        label_7 = new QLabel(tab);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout_2->setWidget(12, QFormLayout::SpanningRole, label_7);

        label_27 = new QLabel(tab);
        label_27->setObjectName(QStringLiteral("label_27"));

        formLayout_2->setWidget(13, QFormLayout::LabelRole, label_27);

        lineEdit_16 = new QLineEdit(tab);
        lineEdit_16->setObjectName(QStringLiteral("lineEdit_16"));
        sizePolicy.setHeightForWidth(lineEdit_16->sizePolicy().hasHeightForWidth());
        lineEdit_16->setSizePolicy(sizePolicy);

        formLayout_2->setWidget(13, QFormLayout::FieldRole, lineEdit_16);

        label_30 = new QLabel(tab);
        label_30->setObjectName(QStringLiteral("label_30"));

        formLayout_2->setWidget(14, QFormLayout::LabelRole, label_30);

        lineEdit_17 = new QLineEdit(tab);
        lineEdit_17->setObjectName(QStringLiteral("lineEdit_17"));
        sizePolicy.setHeightForWidth(lineEdit_17->sizePolicy().hasHeightForWidth());
        lineEdit_17->setSizePolicy(sizePolicy);

        formLayout_2->setWidget(14, QFormLayout::FieldRole, lineEdit_17);

        label_32 = new QLabel(tab);
        label_32->setObjectName(QStringLiteral("label_32"));

        formLayout_2->setWidget(15, QFormLayout::LabelRole, label_32);

        lineEdit_18 = new QLineEdit(tab);
        lineEdit_18->setObjectName(QStringLiteral("lineEdit_18"));
        sizePolicy.setHeightForWidth(lineEdit_18->sizePolicy().hasHeightForWidth());
        lineEdit_18->setSizePolicy(sizePolicy);

        formLayout_2->setWidget(15, QFormLayout::FieldRole, lineEdit_18);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_2->setItem(16, QFormLayout::LabelRole, verticalSpacer_8);

        pushButton_3 = new QPushButton(tab);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        sizePolicy.setHeightForWidth(pushButton_3->sizePolicy().hasHeightForWidth());
        pushButton_3->setSizePolicy(sizePolicy);
        pushButton_3->setFont(font);

        formLayout_2->setWidget(17, QFormLayout::FieldRole, pushButton_3);

        tabWidget->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        formLayout_4 = new QFormLayout(tab_3);
        formLayout_4->setSpacing(6);
        formLayout_4->setContentsMargins(11, 11, 11, 11);
        formLayout_4->setObjectName(QStringLiteral("formLayout_4"));
        label_2 = new QLabel(tab_3);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout_4->setWidget(6, QFormLayout::LabelRole, label_2);

        horizontalSlider = new QSlider(tab_3);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMinimum(0);
        horizontalSlider->setMaximum(360);
        horizontalSlider->setSingleStep(1);
        horizontalSlider->setValue(0);
        horizontalSlider->setSliderPosition(0);
        horizontalSlider->setOrientation(Qt::Horizontal);

        formLayout_4->setWidget(6, QFormLayout::FieldRole, horizontalSlider);

        label_3 = new QLabel(tab_3);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout_4->setWidget(8, QFormLayout::LabelRole, label_3);

        horizontalSlider_2 = new QSlider(tab_3);
        horizontalSlider_2->setObjectName(QStringLiteral("horizontalSlider_2"));
        horizontalSlider_2->setMinimum(0);
        horizontalSlider_2->setMaximum(180);
        horizontalSlider_2->setValue(0);
        horizontalSlider_2->setSliderPosition(0);
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        formLayout_4->setWidget(8, QFormLayout::FieldRole, horizontalSlider_2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        formLayout_4->setItem(9, QFormLayout::LabelRole, verticalSpacer_2);

        label_23 = new QLabel(tab_3);
        label_23->setObjectName(QStringLiteral("label_23"));

        formLayout_4->setWidget(10, QFormLayout::SpanningRole, label_23);

        label_31 = new QLabel(tab_3);
        label_31->setObjectName(QStringLiteral("label_31"));

        formLayout_4->setWidget(12, QFormLayout::LabelRole, label_31);

        horizontalSlider_3 = new QSlider(tab_3);
        horizontalSlider_3->setObjectName(QStringLiteral("horizontalSlider_3"));
        horizontalSlider_3->setMaximum(360);
        horizontalSlider_3->setOrientation(Qt::Horizontal);

        formLayout_4->setWidget(12, QFormLayout::FieldRole, horizontalSlider_3);

        verticalSpacer_14 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_4->setItem(1, QFormLayout::LabelRole, verticalSpacer_14);

        label_22 = new QLabel(tab_3);
        label_22->setObjectName(QStringLiteral("label_22"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_22);

        comboBox_2 = new QComboBox(tab_3);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, comboBox_2);

        label_8 = new QLabel(tab_3);
        label_8->setObjectName(QStringLiteral("label_8"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy2);

        formLayout_4->setWidget(3, QFormLayout::LabelRole, label_8);

        lineEdit = new QLineEdit(tab_3);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        sizePolicy.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
        lineEdit->setSizePolicy(sizePolicy);

        formLayout_4->setWidget(3, QFormLayout::FieldRole, lineEdit);

        label_6 = new QLabel(tab_3);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_4->setWidget(2, QFormLayout::SpanningRole, label_6);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_4->setItem(4, QFormLayout::LabelRole, verticalSpacer);

        label_33 = new QLabel(tab_3);
        label_33->setObjectName(QStringLiteral("label_33"));

        formLayout_4->setWidget(5, QFormLayout::SpanningRole, label_33);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        formLayout_5 = new QFormLayout(tab_4);
        formLayout_5->setSpacing(6);
        formLayout_5->setContentsMargins(11, 11, 11, 11);
        formLayout_5->setObjectName(QStringLiteral("formLayout_5"));
        label = new QLabel(tab_4);
        label->setObjectName(QStringLiteral("label"));

        formLayout_5->setWidget(1, QFormLayout::LabelRole, label);

        spinBox = new QSpinBox(tab_4);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMinimum(1);

        formLayout_5->setWidget(1, QFormLayout::FieldRole, spinBox);

        label_5 = new QLabel(tab_4);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_5->setWidget(2, QFormLayout::LabelRole, label_5);

        pushButton_4 = new QPushButton(tab_4);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        formLayout_5->setWidget(2, QFormLayout::FieldRole, pushButton_4);

        verticalSpacer_10 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_5->setItem(3, QFormLayout::LabelRole, verticalSpacer_10);

        label_11 = new QLabel(tab_4);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout_5->setWidget(4, QFormLayout::LabelRole, label_11);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(15);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(-1, 0, 0, -1);
        label_14 = new QLabel(tab_4);
        label_14->setObjectName(QStringLiteral("label_14"));
        sizePolicy.setHeightForWidth(label_14->sizePolicy().hasHeightForWidth());
        label_14->setSizePolicy(sizePolicy);

        horizontalLayout_8->addWidget(label_14);

        label_15 = new QLabel(tab_4);
        label_15->setObjectName(QStringLiteral("label_15"));
        sizePolicy.setHeightForWidth(label_15->sizePolicy().hasHeightForWidth());
        label_15->setSizePolicy(sizePolicy);

        horizontalLayout_8->addWidget(label_15);

        label_16 = new QLabel(tab_4);
        label_16->setObjectName(QStringLiteral("label_16"));
        sizePolicy.setHeightForWidth(label_16->sizePolicy().hasHeightForWidth());
        label_16->setSizePolicy(sizePolicy);

        horizontalLayout_8->addWidget(label_16);


        formLayout_5->setLayout(5, QFormLayout::SpanningRole, horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(-1, 0, 0, -1);
        lineEdit_6 = new QLineEdit(tab_4);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));
        sizePolicy.setHeightForWidth(lineEdit_6->sizePolicy().hasHeightForWidth());
        lineEdit_6->setSizePolicy(sizePolicy);
        lineEdit_6->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_9->addWidget(lineEdit_6);

        lineEdit_7 = new QLineEdit(tab_4);
        lineEdit_7->setObjectName(QStringLiteral("lineEdit_7"));
        sizePolicy.setHeightForWidth(lineEdit_7->sizePolicy().hasHeightForWidth());
        lineEdit_7->setSizePolicy(sizePolicy);
        lineEdit_7->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_9->addWidget(lineEdit_7);

        lineEdit_8 = new QLineEdit(tab_4);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));
        sizePolicy.setHeightForWidth(lineEdit_8->sizePolicy().hasHeightForWidth());
        lineEdit_8->setSizePolicy(sizePolicy);
        lineEdit_8->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_9->addWidget(lineEdit_8);


        formLayout_5->setLayout(6, QFormLayout::SpanningRole, horizontalLayout_9);

        verticalSpacer_13 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_5->setItem(7, QFormLayout::LabelRole, verticalSpacer_13);

        label_17 = new QLabel(tab_4);
        label_17->setObjectName(QStringLiteral("label_17"));

        formLayout_5->setWidget(8, QFormLayout::LabelRole, label_17);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(15);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(-1, 0, 0, -1);
        label_18 = new QLabel(tab_4);
        label_18->setObjectName(QStringLiteral("label_18"));
        sizePolicy1.setHeightForWidth(label_18->sizePolicy().hasHeightForWidth());
        label_18->setSizePolicy(sizePolicy1);

        horizontalLayout_10->addWidget(label_18);

        label_19 = new QLabel(tab_4);
        label_19->setObjectName(QStringLiteral("label_19"));
        sizePolicy1.setHeightForWidth(label_19->sizePolicy().hasHeightForWidth());
        label_19->setSizePolicy(sizePolicy1);

        horizontalLayout_10->addWidget(label_19);

        label_20 = new QLabel(tab_4);
        label_20->setObjectName(QStringLiteral("label_20"));
        sizePolicy1.setHeightForWidth(label_20->sizePolicy().hasHeightForWidth());
        label_20->setSizePolicy(sizePolicy1);

        horizontalLayout_10->addWidget(label_20);


        formLayout_5->setLayout(9, QFormLayout::SpanningRole, horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(-1, 0, 0, -1);
        lineEdit_9 = new QLineEdit(tab_4);
        lineEdit_9->setObjectName(QStringLiteral("lineEdit_9"));
        sizePolicy.setHeightForWidth(lineEdit_9->sizePolicy().hasHeightForWidth());
        lineEdit_9->setSizePolicy(sizePolicy);
        lineEdit_9->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_11->addWidget(lineEdit_9);

        lineEdit_10 = new QLineEdit(tab_4);
        lineEdit_10->setObjectName(QStringLiteral("lineEdit_10"));
        sizePolicy.setHeightForWidth(lineEdit_10->sizePolicy().hasHeightForWidth());
        lineEdit_10->setSizePolicy(sizePolicy);
        lineEdit_10->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_11->addWidget(lineEdit_10);

        lineEdit_11 = new QLineEdit(tab_4);
        lineEdit_11->setObjectName(QStringLiteral("lineEdit_11"));
        sizePolicy.setHeightForWidth(lineEdit_11->sizePolicy().hasHeightForWidth());
        lineEdit_11->setSizePolicy(sizePolicy);
        lineEdit_11->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_11->addWidget(lineEdit_11);


        formLayout_5->setLayout(10, QFormLayout::SpanningRole, horizontalLayout_11);

        verticalSpacer_11 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_5->setItem(11, QFormLayout::LabelRole, verticalSpacer_11);

        label_24 = new QLabel(tab_4);
        label_24->setObjectName(QStringLiteral("label_24"));
        sizePolicy1.setHeightForWidth(label_24->sizePolicy().hasHeightForWidth());
        label_24->setSizePolicy(sizePolicy1);

        formLayout_5->setWidget(12, QFormLayout::LabelRole, label_24);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_2->setContentsMargins(-1, -1, 10, -1);
        lineEdit_12 = new QLineEdit(tab_4);
        lineEdit_12->setObjectName(QStringLiteral("lineEdit_12"));
        lineEdit_12->setEnabled(true);
        sizePolicy.setHeightForWidth(lineEdit_12->sizePolicy().hasHeightForWidth());
        lineEdit_12->setSizePolicy(sizePolicy);
        lineEdit_12->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_2->addWidget(lineEdit_12);

        lineEdit_19 = new QLineEdit(tab_4);
        lineEdit_19->setObjectName(QStringLiteral("lineEdit_19"));
        sizePolicy.setHeightForWidth(lineEdit_19->sizePolicy().hasHeightForWidth());
        lineEdit_19->setSizePolicy(sizePolicy);
        lineEdit_19->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_2->addWidget(lineEdit_19);

        lineEdit_20 = new QLineEdit(tab_4);
        lineEdit_20->setObjectName(QStringLiteral("lineEdit_20"));
        sizePolicy.setHeightForWidth(lineEdit_20->sizePolicy().hasHeightForWidth());
        lineEdit_20->setSizePolicy(sizePolicy);
        lineEdit_20->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_2->addWidget(lineEdit_20);


        formLayout_5->setLayout(12, QFormLayout::FieldRole, horizontalLayout_2);

        label_25 = new QLabel(tab_4);
        label_25->setObjectName(QStringLiteral("label_25"));
        sizePolicy1.setHeightForWidth(label_25->sizePolicy().hasHeightForWidth());
        label_25->setSizePolicy(sizePolicy1);

        formLayout_5->setWidget(13, QFormLayout::LabelRole, label_25);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, -1, 10, -1);
        lineEdit_13 = new QLineEdit(tab_4);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));
        sizePolicy.setHeightForWidth(lineEdit_13->sizePolicy().hasHeightForWidth());
        lineEdit_13->setSizePolicy(sizePolicy);
        lineEdit_13->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_4->addWidget(lineEdit_13);

        lineEdit_21 = new QLineEdit(tab_4);
        lineEdit_21->setObjectName(QStringLiteral("lineEdit_21"));
        sizePolicy.setHeightForWidth(lineEdit_21->sizePolicy().hasHeightForWidth());
        lineEdit_21->setSizePolicy(sizePolicy);
        lineEdit_21->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_4->addWidget(lineEdit_21);

        lineEdit_22 = new QLineEdit(tab_4);
        lineEdit_22->setObjectName(QStringLiteral("lineEdit_22"));
        sizePolicy.setHeightForWidth(lineEdit_22->sizePolicy().hasHeightForWidth());
        lineEdit_22->setSizePolicy(sizePolicy);
        lineEdit_22->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_4->addWidget(lineEdit_22);


        formLayout_5->setLayout(13, QFormLayout::FieldRole, horizontalLayout_4);

        label_29 = new QLabel(tab_4);
        label_29->setObjectName(QStringLiteral("label_29"));
        sizePolicy1.setHeightForWidth(label_29->sizePolicy().hasHeightForWidth());
        label_29->setSizePolicy(sizePolicy1);

        formLayout_5->setWidget(14, QFormLayout::LabelRole, label_29);

        label_26 = new QLabel(tab_4);
        label_26->setObjectName(QStringLiteral("label_26"));
        sizePolicy1.setHeightForWidth(label_26->sizePolicy().hasHeightForWidth());
        label_26->setSizePolicy(sizePolicy1);

        formLayout_5->setWidget(15, QFormLayout::LabelRole, label_26);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(-1, -1, 10, -1);
        lineEdit_14 = new QLineEdit(tab_4);
        lineEdit_14->setObjectName(QStringLiteral("lineEdit_14"));
        sizePolicy.setHeightForWidth(lineEdit_14->sizePolicy().hasHeightForWidth());
        lineEdit_14->setSizePolicy(sizePolicy);
        lineEdit_14->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_5->addWidget(lineEdit_14);

        lineEdit_24 = new QLineEdit(tab_4);
        lineEdit_24->setObjectName(QStringLiteral("lineEdit_24"));
        sizePolicy.setHeightForWidth(lineEdit_24->sizePolicy().hasHeightForWidth());
        lineEdit_24->setSizePolicy(sizePolicy);
        lineEdit_24->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_5->addWidget(lineEdit_24);

        lineEdit_23 = new QLineEdit(tab_4);
        lineEdit_23->setObjectName(QStringLiteral("lineEdit_23"));
        sizePolicy.setHeightForWidth(lineEdit_23->sizePolicy().hasHeightForWidth());
        lineEdit_23->setSizePolicy(sizePolicy);
        lineEdit_23->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_5->addWidget(lineEdit_23);


        formLayout_5->setLayout(15, QFormLayout::FieldRole, horizontalLayout_5);

        verticalSpacer_12 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_5->setItem(16, QFormLayout::LabelRole, verticalSpacer_12);

        label_28 = new QLabel(tab_4);
        label_28->setObjectName(QStringLiteral("label_28"));

        formLayout_5->setWidget(17, QFormLayout::LabelRole, label_28);

        comboBox_3 = new QComboBox(tab_4);
        comboBox_3->setObjectName(QStringLiteral("comboBox_3"));

        formLayout_5->setWidget(17, QFormLayout::FieldRole, comboBox_3);

        verticalSpacer_9 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_5->setItem(18, QFormLayout::LabelRole, verticalSpacer_9);

        radioButton_4 = new QRadioButton(tab_4);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));

        formLayout_5->setWidget(19, QFormLayout::LabelRole, radioButton_4);

        radioButton_3 = new QRadioButton(tab_4);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setChecked(true);

        formLayout_5->setWidget(19, QFormLayout::FieldRole, radioButton_3);

        lineEdit_15 = new QLineEdit(tab_4);
        lineEdit_15->setObjectName(QStringLiteral("lineEdit_15"));
        sizePolicy.setHeightForWidth(lineEdit_15->sizePolicy().hasHeightForWidth());
        lineEdit_15->setSizePolicy(sizePolicy);
        lineEdit_15->setMaximumSize(QSize(128, 16777215));

        formLayout_5->setWidget(14, QFormLayout::FieldRole, lineEdit_15);

        tabWidget->addTab(tab_4, QString());
        label_17->raise();
        label_28->raise();
        comboBox_3->raise();
        label_25->raise();
        label_26->raise();
        label_24->raise();
        label->raise();
        label_11->raise();
        spinBox->raise();
        label_29->raise();
        label_5->raise();
        pushButton_4->raise();
        radioButton_4->raise();
        radioButton_3->raise();
        lineEdit_15->raise();

        verticalLayout_2->addWidget(tabWidget);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_7);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setFont(font);

        verticalLayout_2->addWidget(pushButton);

        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setFont(font);

        verticalLayout_2->addWidget(pushButton_5);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setFont(font);

        verticalLayout_2->addWidget(pushButton_2);


        horizontalLayout->addLayout(verticalLayout_2);


        horizontalLayout_3->addLayout(horizontalLayout);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QStringLiteral("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 828, 640));
        scrollArea->setWidget(scrollAreaWidgetContents_2);

        horizontalLayout_3->addWidget(scrollArea);


        verticalLayout_3->addLayout(horizontalLayout_3);

        vtk_projectClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(vtk_projectClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1172, 21));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        menuHelp_2 = new QMenu(menuBar);
        menuHelp_2->setObjectName(QStringLiteral("menuHelp_2"));
        vtk_projectClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(vtk_projectClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        vtk_projectClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(vtk_projectClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        vtk_projectClass->setStatusBar(statusBar);

        menuBar->addAction(menuHelp->menuAction());
        menuBar->addAction(menuHelp_2->menuAction());
        menuHelp->addAction(actionNew);
        menuHelp->addAction(actionOpen);
        menuHelp->addSeparator();
        menuHelp_2->addAction(actionGenerovanie);
        menuHelp_2->addAction(actionPremietanie);
        menuHelp_2->addAction(actionOsvetlenie);

        retranslateUi(vtk_projectClass);
        QObject::connect(pushButton, SIGNAL(clicked()), vtk_projectClass, SLOT(kresli_click()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), vtk_projectClass, SLOT(vymaz_click()));
        QObject::connect(actionNew, SIGNAL(triggered()), vtk_projectClass, SLOT(new_img()));
        QObject::connect(actionOpen, SIGNAL(triggered()), vtk_projectClass, SLOT(open()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), vtk_projectClass, SLOT(generuj_click()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), vtk_projectClass, SLOT(zadaj_polohu_svetla_click()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), vtk_projectClass, SLOT(povodne_hodnoty_click()));
        QObject::connect(actionGenerovanie, SIGNAL(triggered()), vtk_projectClass, SLOT(help_generovanie()));
        QObject::connect(actionPremietanie, SIGNAL(triggered()), vtk_projectClass, SLOT(help_premietanie()));
        QObject::connect(actionOsvetlenie, SIGNAL(triggered()), vtk_projectClass, SLOT(help_osvetlenie()));

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(vtk_projectClass);
    } // setupUi

    void retranslateUi(QMainWindow *vtk_projectClass)
    {
        vtk_projectClass->setWindowTitle(QApplication::translate("vtk_projectClass", "vtk_project", 0));
        actionHelp->setText(QApplication::translate("vtk_projectClass", "Help", 0));
        actionNew->setText(QApplication::translate("vtk_projectClass", "New", 0));
        actionOpen->setText(QApplication::translate("vtk_projectClass", "Open", 0));
        actionGenerovanie->setText(QApplication::translate("vtk_projectClass", "Generovanie", 0));
        actionPremietanie->setText(QApplication::translate("vtk_projectClass", "Premietanie", 0));
        actionOsvetlenie->setText(QApplication::translate("vtk_projectClass", "Osvetlenie", 0));
        label_4->setText(QApplication::translate("vtk_projectClass", "Vygeneruje sa:", 0));
        radioButton_2->setText(QApplication::translate("vtk_projectClass", "Elipsoid", 0));
        radioButton->setText(QApplication::translate("vtk_projectClass", "Bezierova plocha", 0));
        label_21->setText(QApplication::translate("vtk_projectClass", "Parametre elipsoidu:", 0));
        label_10->setText(QApplication::translate("vtk_projectClass", "Dlzka prvej poloosi:", 0));
        lineEdit_5->setText(QApplication::translate("vtk_projectClass", "5", 0));
        label_9->setText(QApplication::translate("vtk_projectClass", "Dlzka druhej poloosi:", 0));
        lineEdit_4->setText(QApplication::translate("vtk_projectClass", "10", 0));
        label_12->setText(QApplication::translate("vtk_projectClass", "Pocet rovnobeziek:", 0));
        lineEdit_3->setText(QApplication::translate("vtk_projectClass", "5", 0));
        label_13->setText(QApplication::translate("vtk_projectClass", "Pocet poludnikov:", 0));
        lineEdit_2->setText(QApplication::translate("vtk_projectClass", "5", 0));
        label_7->setText(QApplication::translate("vtk_projectClass", "Parametre Bezierovej plochy:", 0));
        label_27->setText(QApplication::translate("vtk_projectClass", "Dlzka vektora posunutia:", 0));
        lineEdit_16->setText(QApplication::translate("vtk_projectClass", "100", 0));
        label_30->setText(QApplication::translate("vtk_projectClass", "Pocet deleni vektora:", 0));
        lineEdit_17->setText(QApplication::translate("vtk_projectClass", "10", 0));
        label_32->setText(QApplication::translate("vtk_projectClass", "Pocet deleni krivky:", 0));
        lineEdit_18->setText(QApplication::translate("vtk_projectClass", "10", 0));
        pushButton_3->setText(QApplication::translate("vtk_projectClass", "Generuj", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("vtk_projectClass", "Generovanie", 0));
        label_2->setText(QApplication::translate("vtk_projectClass", "Azimut:", 0));
        label_3->setText(QApplication::translate("vtk_projectClass", "Zenit:", 0));
        label_23->setText(QApplication::translate("vtk_projectClass", "Geometricka transformacia:", 0));
        label_31->setText(QApplication::translate("vtk_projectClass", "Rotacia:", 0));
        label_22->setText(QApplication::translate("vtk_projectClass", "Premietanie:", 0));
        comboBox_2->clear();
        comboBox_2->insertItems(0, QStringList()
         << QApplication::translate("vtk_projectClass", "Rovnobezne", 0)
         << QApplication::translate("vtk_projectClass", "Stredove", 0)
        );
        label_8->setText(QApplication::translate("vtk_projectClass", "Vzdialenost:", 0));
        lineEdit->setText(QApplication::translate("vtk_projectClass", "300", 0));
        label_6->setText(QApplication::translate("vtk_projectClass", "Pre stredove premietanie:", 0));
        label_33->setText(QApplication::translate("vtk_projectClass", "Natacanie:", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("vtk_projectClass", "Premietanie", 0));
        label->setText(QApplication::translate("vtk_projectClass", "Pocet zdrojov svetla:", 0));
        label_5->setText(QApplication::translate("vtk_projectClass", "Poloha zdroja:", 0));
        pushButton_4->setText(QApplication::translate("vtk_projectClass", "Zadanie polohy", 0));
        label_11->setText(QApplication::translate("vtk_projectClass", "Farba svetla zo zdroja:", 0));
        label_14->setText(QApplication::translate("vtk_projectClass", "Red", 0));
        label_15->setText(QApplication::translate("vtk_projectClass", "Green", 0));
        label_16->setText(QApplication::translate("vtk_projectClass", "Blue", 0));
        lineEdit_6->setText(QApplication::translate("vtk_projectClass", "255", 0));
        lineEdit_7->setText(QApplication::translate("vtk_projectClass", "0", 0));
        lineEdit_8->setText(QApplication::translate("vtk_projectClass", "0", 0));
        label_17->setText(QApplication::translate("vtk_projectClass", "Farba okoliteho svetla:", 0));
        label_18->setText(QApplication::translate("vtk_projectClass", "Red", 0));
        label_19->setText(QApplication::translate("vtk_projectClass", "Green", 0));
        label_20->setText(QApplication::translate("vtk_projectClass", "Blue", 0));
        lineEdit_9->setText(QApplication::translate("vtk_projectClass", "255", 0));
        lineEdit_10->setText(QApplication::translate("vtk_projectClass", "255", 0));
        lineEdit_11->setText(QApplication::translate("vtk_projectClass", "255", 0));
        label_24->setText(QApplication::translate("vtk_projectClass", "Koeficient difuzie:", 0));
        lineEdit_12->setText(QApplication::translate("vtk_projectClass", "0.2", 0));
        lineEdit_19->setText(QApplication::translate("vtk_projectClass", "0", 0));
        lineEdit_20->setText(QApplication::translate("vtk_projectClass", "0", 0));
        label_25->setText(QApplication::translate("vtk_projectClass", "Koeficient odrazu:", 0));
        lineEdit_13->setText(QApplication::translate("vtk_projectClass", "0.2", 0));
        lineEdit_21->setText(QApplication::translate("vtk_projectClass", "0.2", 0));
        lineEdit_22->setText(QApplication::translate("vtk_projectClass", "0.2", 0));
        label_29->setText(QApplication::translate("vtk_projectClass", "Ostrost:", 0));
        label_26->setText(QApplication::translate("vtk_projectClass", "Ambientny koeficient:", 0));
        lineEdit_14->setText(QApplication::translate("vtk_projectClass", "0.2", 0));
        lineEdit_24->setText(QApplication::translate("vtk_projectClass", "0.2", 0));
        lineEdit_23->setText(QApplication::translate("vtk_projectClass", "0.2", 0));
        label_28->setText(QApplication::translate("vtk_projectClass", "Tienovanie:", 0));
        comboBox_3->clear();
        comboBox_3->insertItems(0, QStringList()
         << QApplication::translate("vtk_projectClass", "Konstantne", 0)
         << QApplication::translate("vtk_projectClass", "Gouraudovo", 0)
        );
        radioButton_4->setText(QApplication::translate("vtk_projectClass", "Osvetlenie", 0));
        radioButton_3->setText(QApplication::translate("vtk_projectClass", "Wireframe", 0));
        lineEdit_15->setText(QApplication::translate("vtk_projectClass", "5", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("vtk_projectClass", "Osvetlenie", 0));
        pushButton->setText(QApplication::translate("vtk_projectClass", "Kresli", 0));
        pushButton_5->setText(QApplication::translate("vtk_projectClass", "Povodne hodnoty", 0));
        pushButton_2->setText(QApplication::translate("vtk_projectClass", "Vymaz", 0));
        menuHelp->setTitle(QApplication::translate("vtk_projectClass", "Options", 0));
        menuHelp_2->setTitle(QApplication::translate("vtk_projectClass", "Help", 0));
    } // retranslateUi

};

namespace Ui {
    class vtk_projectClass: public Ui_vtk_projectClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VTK_PROJECT_H
