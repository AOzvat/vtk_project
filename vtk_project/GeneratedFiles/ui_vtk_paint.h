/********************************************************************************
** Form generated from reading UI file 'vtk_paint.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VTK_PAINT_H
#define UI_VTK_PAINT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_vtk_paint
{
public:

    void setupUi(QWidget *vtk_paint)
    {
        if (vtk_paint->objectName().isEmpty())
            vtk_paint->setObjectName(QStringLiteral("vtk_paint"));
        vtk_paint->resize(400, 300);

        retranslateUi(vtk_paint);

        QMetaObject::connectSlotsByName(vtk_paint);
    } // setupUi

    void retranslateUi(QWidget *vtk_paint)
    {
        vtk_paint->setWindowTitle(QApplication::translate("vtk_paint", "vtk_paint", 0));
    } // retranslateUi

};

namespace Ui {
    class vtk_paint: public Ui_vtk_paint {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VTK_PAINT_H
